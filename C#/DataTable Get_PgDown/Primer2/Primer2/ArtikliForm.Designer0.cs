﻿namespace Primer2
{
  partial class ArtikliForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose();
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.labelFilter = new System.Windows.Forms.Label();
      this.dgv = new System.Windows.Forms.DataGridView();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.meniArtikal = new System.Windows.Forms.ToolStripDropDownButton();
      this.meniArtikalDodaj = new System.Windows.Forms.ToolStripMenuItem();
      this.meniArtikalIzmeni = new System.Windows.Forms.ToolStripMenuItem();
      this.meniArtikalObrisi = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.meniArtikalNazad = new System.Windows.Forms.ToolStripMenuItem();
      this.meniArtikliOsvezi = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.meniArtikliSort = new System.Windows.Forms.ToolStripMenuItem();
      ( (System.ComponentModel.ISupportInitialize) ( this.dgv ) ).BeginInit();
      this.toolStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // labelFilter
      // 
      this.labelFilter.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.labelFilter.Location = new System.Drawing.Point( 0, 488 );
      this.labelFilter.Name = "labelFilter";
      this.labelFilter.Size = new System.Drawing.Size( 757, 22 );
      this.labelFilter.TabIndex = 2;
      // 
      // dgv
      // 
      this.dgv.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
                  | System.Windows.Forms.AnchorStyles.Left )
                  | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgv.Location = new System.Drawing.Point( 0, 26 );
      this.dgv.Name = "dgv";
      this.dgv.RowTemplate.Height = 24;
      this.dgv.Size = new System.Drawing.Size( 757, 459 );
      this.dgv.TabIndex = 1;
      // 
      // toolStrip1
      // 
      this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
      this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.meniArtikal} );
      this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
      this.toolStrip1.ShowItemToolTips = false;
      this.toolStrip1.Size = new System.Drawing.Size( 757, 25 );
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // meniArtikal
      // 
      this.meniArtikal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.meniArtikal.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.meniArtikalDodaj,
            this.meniArtikalIzmeni,
            this.meniArtikalObrisi,
            this.toolStripSeparator2,
            this.meniArtikliSort,
            this.meniArtikliOsvezi,
            this.toolStripSeparator1,
            this.meniArtikalNazad} );
      this.meniArtikal.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.meniArtikal.Name = "meniArtikal";
      this.meniArtikal.ShowDropDownArrow = false;
      this.meniArtikal.Size = new System.Drawing.Size( 47, 22 );
      this.meniArtikal.Text = "&Artikli";
      // 
      // meniArtikalDodaj
      // 
      this.meniArtikalDodaj.Name = "meniArtikalDodaj";
      this.meniArtikalDodaj.ShortcutKeyDisplayString = "Ins";
      this.meniArtikalDodaj.Size = new System.Drawing.Size( 166, 22 );
      this.meniArtikalDodaj.Text = "&Dodaj";
      this.meniArtikalDodaj.Click += new System.EventHandler( this.clickMeniDodaj );
      // 
      // meniArtikalIzmeni
      // 
      this.meniArtikalIzmeni.Name = "meniArtikalIzmeni";
      this.meniArtikalIzmeni.ShortcutKeyDisplayString = "Enter";
      this.meniArtikalIzmeni.Size = new System.Drawing.Size( 166, 22 );
      this.meniArtikalIzmeni.Text = "&Izmeni";
      this.meniArtikalIzmeni.Click += new System.EventHandler( this.clickMeniIzmeni );
      // 
      // meniArtikalObrisi
      // 
      this.meniArtikalObrisi.Name = "meniArtikalObrisi";
      this.meniArtikalObrisi.ShortcutKeyDisplayString = "Del";
      this.meniArtikalObrisi.Size = new System.Drawing.Size( 166, 22 );
      this.meniArtikalObrisi.Text = "&Obriši";
      this.meniArtikalObrisi.Click += new System.EventHandler( this.clickMeniObrisi );
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size( 163, 6 );
      // 
      // meniArtikalNazad
      // 
      this.meniArtikalNazad.Name = "meniArtikalNazad";
      this.meniArtikalNazad.ShortcutKeyDisplayString = "Esc";
      this.meniArtikalNazad.Size = new System.Drawing.Size( 166, 22 );
      this.meniArtikalNazad.Text = "&Nazad";
      this.meniArtikalNazad.Click += new System.EventHandler( this.clickMeniNazad );
      // 
      // meniArtikliOsvezi
      // 
      this.meniArtikliOsvezi.Name = "meniArtikliOsvezi";
      this.meniArtikliOsvezi.ShortcutKeyDisplayString = "F5";
      this.meniArtikliOsvezi.Size = new System.Drawing.Size( 166, 22 );
      this.meniArtikliOsvezi.Text = "Os&veži";
      this.meniArtikliOsvezi.Click += new System.EventHandler( this.clickMeniOsvezi );
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size( 163, 6 );
      // 
      // meniArtikliSort
      // 
      this.meniArtikliSort.Name = "meniArtikliSort";
      this.meniArtikliSort.ShortcutKeyDisplayString = "F2";
      this.meniArtikliSort.Size = new System.Drawing.Size( 166, 22 );
      this.meniArtikliSort.Text = "&Sort";
      this.meniArtikliSort.Click += new System.EventHandler( this.clickMeniSort );
      // 
      // ArtikliForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF( 8F, 16F );
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size( 757, 510 );
      this.Controls.Add( this.labelFilter );
      this.Controls.Add( this.dgv );
      this.Controls.Add( this.toolStrip1 );
      this.Name = "ArtikliForm";
      this.Text = "Artikli";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler( this.formClosed );
      this.Load += new System.EventHandler( this.formLoad );
      ( (System.ComponentModel.ISupportInitialize) ( this.dgv ) ).EndInit();
      this.toolStrip1.ResumeLayout( false );
      this.toolStrip1.PerformLayout();
      this.ResumeLayout( false );
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripDropDownButton meniArtikal;
    private System.Windows.Forms.ToolStripMenuItem meniArtikalDodaj;
    private System.Windows.Forms.ToolStripMenuItem meniArtikalIzmeni;
    private System.Windows.Forms.ToolStripMenuItem meniArtikalObrisi;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem meniArtikalNazad;
    private System.Windows.Forms.DataGridView dgv;
    private System.Windows.Forms.Label labelFilter;
    private System.Windows.Forms.ToolStripMenuItem meniArtikliOsvezi;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem meniArtikliSort;
  }
}