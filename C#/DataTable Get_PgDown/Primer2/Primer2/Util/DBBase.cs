using System;
using System.Data;
using System.Data.Odbc;
using System.Globalization;

public class DBBase : IDisposable
{
  protected const int DefaultCommandTimeout = 45;


  class ConnectionState
  {
    public String ConnectionString { get; set; }
    public OdbcConnection Connection { get; set; }
    public OdbcTransaction Transaction { get; set; }
    public int ErrorCode { get; set; }
    public String ErrorMessage { get; set; }
    public String LastSqlCommand { get; set; }
    public Action OnAfterConnect { get; set; }
  }


  ConnectionState m_state;
  CultureInfo m_serbian_latin_culture_info;



  protected DBBase()
  {
    m_state = new ConnectionState();
    m_state.OnAfterConnect = OnAfterConnect;

    m_serbian_latin_culture_info = null;

    CultureInfo[] cultures = CultureInfo.GetCultures( CultureTypes.AllCultures );

    foreach ( CultureInfo culture in cultures )
    {
      String name = culture.Name.ToUpperInvariant();

      if ( name == "SR-LATN-RS" )
      {
        m_serbian_latin_culture_info = culture;
        break;
      }
    }

    if ( m_serbian_latin_culture_info == null )
    {
      foreach ( CultureInfo culture in cultures )
      {
        String name = culture.Name.ToUpperInvariant();

        if ( name == "SR-LATN-CS" )
        {
          m_serbian_latin_culture_info = culture;
          break;
        }
      }

      if ( m_serbian_latin_culture_info == null )
      {
        throw new Exception( "Nema ni sr-Latn-RS, ni sr-Latn-CS kulture." );
      }
    }
  }


  protected DBBase( String connection_string ) : this()
  {
    Initialize( connection_string );
  }


  ~DBBase()
  {
    Dispose( false );
  }


  /// <summary>
  /// Vraca Serbian Latin CultureInfo objekat (generalno za potrebe sortiranja).
  /// </summary>
  public CultureInfo SerbianLatinCultureInfo { get { return m_serbian_latin_culture_info; } }


  /// <summary>
  /// Predaje svoju konekciju ka izvoru podataka drugom DBBase baziranom objektu.
  /// Nakon izvrsenja, oba objekta imaju istu konekciju ka izvoru podataka.
  /// </summary>
  /// <param name="db">DBBase bazirani objekat koji ce primiti konekciju.</param>
  public void ShareConnectionState( DBBase db )
  {
    if ( !ReferenceEquals( db.m_state, m_state ) )
    {
      Disconnect();
      db.Disconnect();
      db.m_state.OnAfterConnect -= db.OnAfterConnect;
      db.m_state = m_state;
      db.m_state.OnAfterConnect += db.OnAfterConnect;
    }
  }


  /// <summary>
  /// Inicijalizuje DBBase bazirani objekat sa zadatim konekcionim stringom.
  /// Ova metoda mora biti pozvana pre prvog poziva metode Connect.
  /// </summary>
  /// <param name="connection_string">Konekcioni string.</param>
  protected void Initialize( String connection_string )
  {
    m_state.Connection = null;
    m_state.Transaction = null;
    m_state.ConnectionString = connection_string;

    ErrorCode = 0;
    ErrorMessage = "";
    LastSqlCommand = "";
  }


  /// <summary>
  /// Kod (broj) greske zadnje izvrsene komande.
  /// </summary>
  public int ErrorCode
  {
    get { return m_state.ErrorCode; }
    protected set { m_state.ErrorCode = value; }
  }

  /// <summary>
  /// Tekstualni opis greske zadnje izvrsene komande.
  /// </summary>
  public String ErrorMessage
  {
    get { return m_state.ErrorMessage; }
    protected set { m_state.ErrorMessage = value; }
  }


  /// <summary>
  /// SQL tekst zadnje izvrsene komande.
  /// </summary>
  protected String LastSqlCommand
  {
    get { return m_state.LastSqlCommand; }
    set { m_state.LastSqlCommand = value; }
  }


  /// <summary>
  /// Zatvara konekciju ka izvoru podataka i deinstalira povezane dogadjaje.
  /// Nakon izvrsenja ove metode vise ne bi trebalo upotrebljavati objekat.
  /// Nikad ne baca exception.
  /// </summary>
  public void Dispose()
  {
    Dispose( true );
  }


  protected virtual void Dispose( bool disposing )
  {
    if ( disposing )
    {
      m_state.OnAfterConnect -= OnAfterConnect;
      Disconnect();
    }
  }


  /// <summary>
  /// Dogadjaj biva izazvan svaki put kad se uspostavi konekcija sa izvorom podataka.
  /// Ukoliko postoji bitno podesavanje koje se gubi ako se veza prekine, sa ovim
  /// dogadjajem se moze podesiti da se nesto izvrsi svaki put kad se konekcija uspostavi
  /// ponovo.
  /// </summary>
  protected event EventHandler AfterConnect;


  /// <summary>
  /// Metoda biva pozvana svaki put kad se uspostavi konekcija sa izvorom podataka.
  /// Ukoliko postoji bitno podesavanje koje se gubi ako se veza prekine, kreiranjem
  /// override-a ove virtuelne metode u izvedenoj klasi se moze podesiti da se nesto
  /// izvrsi svaki put kad se konekcija uspostavi ponovo.
  /// </summary>
  protected virtual void OnAfterConnect()
  {
    if ( AfterConnect != null ) AfterConnect( this, EventArgs.Empty );
  }

  /// <summary>
  /// Uspostavlja vezu sa izvorom podataka. Ako nastupi greska, vraca false i detalji
  /// problema se mogu dobiti iz ErrorCode i ErrorMessage. Konstruktor sa konekcionim
  /// stringom ili metoda Initialize kojom se postavlja konekcioni string mora biti pozvana
  /// pre poziva ove metode.
  /// </summary>
  protected bool Connect()
  {
    ErrorMessage = "";
    ErrorCode = 0;

    if ( m_state.Connection == null )
    {
      try
      {
        m_state.Connection = new OdbcConnection();
        m_state.Connection.ConnectionString = m_state.ConnectionString;
        m_state.Connection.Open();

        m_state.Transaction = null;

        if ( m_state.OnAfterConnect != null ) m_state.OnAfterConnect();
      }
      catch ( Exception ex )
      {
        OdbcException oe_ex = ex as OdbcException;

        if ( oe_ex != null )
        {
          ErrorMessage = "";
          ErrorCode = -1;

          foreach ( OdbcError error in oe_ex.Errors )
          {
            if ( ErrorCode == -1 ) ErrorCode = error.NativeError;
            if ( ErrorMessage.Length > 0 ) ErrorMessage += "\r\n\r\n";
            ErrorMessage += error.Message;
          }
        }
        else
        {
          ErrorMessage = ex.ToString();
          ErrorCode = -1;
        }

        return false;
      }
    }

    return true;
  }

  /// <summary>
  /// Prekida konekciju sa izvorom podataka. Metoda se bezbedno moze
  /// pozvati i ako konekcija uopste nije uspostavljena ili u toku
  /// eksplicitne transakcije. Nikad ne baca exception.
  /// </summary>
  public void Disconnect()
  {
    try
    {
      if ( m_state.Connection != null )
      {
        TransactionRollback();

        m_state.Connection.Close();
        m_state.Connection.Dispose();
      }
    }
    catch
    {
    }

    m_state.Connection = null;
  }


  /// <summary>
  /// Od zadatog stringa pravi ispravanu i za upotrebu bezbednu
  /// SQL string konstantu pogodnu za upotrebu u SQL izrazu, metoda sama dodaje
  /// apostrof simbole na pocetku i kraju. U slucaju da je parametar null ili
  /// DBNull.Value, vraca string "NULL". Baca exception ako
  /// parametar nije niti null, niti DBNull.Value, niti String.
  /// </summary>
  protected String SqlString( Object o )
  {
    if ( o == null ) return "NULL";
    if ( o == DBNull.Value ) return "NULL";

    String value = o as String;

    if ( value != null ) return SqlString( value );

    throw new Exception( "SqlString parametar nije String." );
  }


  /// <summary>
  /// Od zadatog stringa pravi ispravanu i za upotrebu bezbednu
  /// SQL string konstantu pogodnu za upotrebu u SQL izrazu, metoda sama dodaje
  /// apostrof simbole na pocetku i kraju. U slucaju da je parametar null vraca
  /// string "NULL".
  /// </summary>
  protected String SqlString( String value )
  {
    if ( value == null ) return "NULL";

    return "'" + value.Replace( "\\", "\\\\" )
                      .Replace( "\'", "\\\'" )
                      .Replace( "\"", "\\\"" )
                      .Replace( "\0", "\\0" )
                      .Replace( "\n", "\\n" )
                      .Replace( "\r", "\\r" )
                      .Replace( "\u001A", "\\Z" ) + "'";
  }


  /// <summary>
  /// Pravi SQL string konstantu formata 'yyyy-MM-dd' od zadate DateTime
  /// vrednosti pogodnu za upotrebu u SQL izrazu (koristi se samo datumski deo
  /// DateTime vrednosti, vreme u danu se ignorise), metoda sama dodaje apostrof
  /// simbole na pocetku i kraju. U slucaju da je parametar null, DBNull.Value ili
  /// DateTime.MinValue vraca string "NULL". Baca exception ako parametar nije
  /// niti null, niti DBNull.Value, niti DateTime.
  /// </summary>
  protected String SqlDate( Object o )
  {
    if ( o == null ) return "NULL";
    if ( o == DBNull.Value ) return "NULL";

    if ( o is DateTime ) return SqlDate( (DateTime) o );

    throw new Exception( "SqlDate parametar nije DateTime." );
  }


  /// <summary>
  /// Pravi SQL string konstantu formata 'yyyy-MM-dd' od zadate DateTime
  /// vrednosti pogodnu za upotrebu u SQL izrazu (koristi se samo datumski deo
  /// DateTime vrednosti, vreme u danu se ignorise), metoda sama dodaje apostrof
  /// simbole na pocetku i kraju. U slucaju da je parametar DateTime.MinValue
  /// vraca string "NULL".
  /// </summary>
  protected String SqlDate( DateTime value )
  {
    if ( value == DateTime.MinValue ) return "NULL";

    return "'" + value.Year.ToString( "D4" ) + "-" +
                 value.Month.ToString( "D2" ) + "-" +
                 value.Day.ToString( "D2" ) + "'";
  }


  /// <summary>
  /// Pravi SQL string konstantu formata 'yyyy-MM-dd HH:mm:ss' od zadate DateTime
  /// vrednosti pogodnu za upotrebu u SQL izrazu, metoda sama dodaje apostrof simbole
  /// na pocetku i kraju. U slucaju da je parametar null, DBNull.Value ili
  /// DateTime.MinValue vraca string "NULL".
  /// Baca exception ako parametar nije niti null, niti DBNull.Value, niti DateTime.
  /// </summary>
  protected String SqlDateTime( Object o )
  {
    if ( o == null ) return "NULL";
    if ( o == DBNull.Value ) return "NULL";

    if ( o is DateTime ) return SqlDateTime( (DateTime) o );

    throw new Exception( "SqlDateTime parametar nije DateTime." );
  }


  /// <summary>
  /// Pravi SQL string konstantu formata 'yyyy-MM-dd HH:mm:ss' od zadate DateTime
  /// vrednosti pogodnu za upotrebu u SQL izrazu, metoda sama dodaje apostrof simbole
  /// na pocetku i kraju. U slucaju da je parametar DateTime.MinValue vraca string "NULL".
  /// </summary>
  protected String SqlDateTime( DateTime value )
  {
    if ( value == DateTime.MinValue ) return "NULL";

    return "'" + value.Year.ToString( "D4" ) + "-" +
                 value.Month.ToString( "D2" ) + "-" +
                 value.Day.ToString( "D2" ) + " " +
                 value.Hour.ToString( "D2" ) + ":" +
                 value.Minute.ToString( "D2" ) + ":" +
                 value.Second.ToString( "D2" ) + "'";
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate brojne vrednosti.
  /// Ukoliko brojna vrednost ima decimale, numericka konstanta
  /// ce sadrzati podrazumevani broj decimala.
  /// Ako je parametar null ili DBNull.Value, metoda vraca "NULL".
  /// Baca exception ako parametar nije niti null, niti
  /// DBNull.Value, niti je bilo koji od podrzanih numerickih formata.
  /// </summary>
  protected String SqlNumber( Object o )
  {
    if ( o == null ) return "NULL";
    if ( o == DBNull.Value ) return "NULL";

    if ( o is Double ) return SqlNumber( (Double) o );
    if ( o is Decimal ) return SqlNumber( (Decimal) o );
    if ( o is Single ) return SqlNumber( (Double) (Single) o );
    if ( o is Int32 ) return SqlNumber( (Double) (Int32) o );
    if ( o is Int64 ) return SqlNumber( (Double) (Int64) o );
    if ( o is UInt32 ) return SqlNumber( (Double) (UInt32) o );
    if ( o is UInt64 ) return SqlNumber( (Double) (UInt64) o );

    throw new Exception( "SqlNumber parametar nije broj." );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate Decimal vrednosti.
  /// Konstanta ce imati podrazumevan broj decimala.
  /// </summary>
  protected String SqlNumber( Decimal value )
  {
    return value.ToString( CultureInfo.InvariantCulture );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate Double vrednosti.
  /// Konstanta ce imati podrazumevan broj decimala.
  /// </summary>
  protected String SqlNumber( Double value )
  {
    return value.ToString( CultureInfo.InvariantCulture );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate brojne vrednosti.
  /// Konstanta ce imati broj decimala zadat parametrom <paramref name="dec"/>.
  /// Ako je parametar <paramref name="o"/> null ili DBNull.Value, metoda vraca "NULL".
  /// Baca exception ako parametar <paramref name="o"/> nije niti null, niti
  /// DBNull.Value, niti je bilo koji od podrzanih numerickih formata.
  /// </summary>
  /// <param name="o">Brojna vrednost.</param>
  /// <param name="dec">Broj decimala.</param>
  protected String SqlNumber( Object o, int dec )
  {
    if ( o == null ) return "NULL";
    if ( o == DBNull.Value ) return "NULL";

    if ( o is Double ) return SqlNumber( (Double) o, dec );
    if ( o is Decimal ) return SqlNumber( (Decimal) o, dec );
    if ( o is Single ) return SqlNumber( (Double) (Single) o, dec );
    if ( o is Int32 ) return SqlNumber( (Double) (Int32) o, dec );
    if ( o is Int64 ) return SqlNumber( (Double) (Int64) o, dec );
    if ( o is UInt32 ) return SqlNumber( (Double) (UInt32) o, dec );
    if ( o is UInt64 ) return SqlNumber( (Double) (UInt64) o, dec );

    throw new Exception( "SqlNumber parametar nije broj." );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate Decimal vrednosti.
  /// Konstanta ce imati broj decimala zadat parametrom <paramref name="dec"/>.
  /// </summary>
  /// <param name="value">Brojna vrednost.</param>
  /// <param name="dec">Broj decimala.</param>
  protected String SqlNumber( Decimal value, int dec )
  {
    return value.ToString( "F" + dec.ToString(),
                           CultureInfo.InvariantCulture );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate Double vrednosti.
  /// Konstanta ce imati broj decimala zadat parametrom <paramref name="dec"/>.
  /// </summary>
  /// <param name="value">Brojna vrednost.</param>
  /// <param name="dec">Broj decimala.</param>
  protected String SqlNumber( Double value, int dec )
  {
    return value.ToString( "F" + dec.ToString(),
                           CultureInfo.InvariantCulture );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate Boolean vrednosti.
  /// Ako je parametar null ili DBNull.Value, metoda vraca "NULL".
  /// Baca exception ako parametar nije niti null, niti
  /// DBNull.Value, niti Boolean.
  /// </summary>
  protected String SqlBool( Object o )
  {
    if ( o == null ) return "NULL";
    if ( o == DBNull.Value ) return "NULL";

    if ( o is bool ) return SqlBool( (bool) o );

    throw new Exception( "SqlBool parametar nije bool." );
  }


  /// <summary>
  /// Pravi SQL numericku konstantu u vidu stringa pogodnog
  /// za upotrebu u SQL izrazu od zadate Boolean vrednosti.
  /// </summary>
  protected String SqlBool( bool value )
  {
    return value ? "1" : "0";
  }


  /// <summary>
  /// Konvertuje tip zadate vrednosti u tip int,
  /// baca exception ako konverzija nije moguca.
  /// </summary>
  protected int CastToInt( Object o )
  {
    if ( o == null ) throw new Exception( "CastToInt parametar je null." );

    if ( o is int ) return (int) o;
    else if ( o is Int64 ) return (int) (Int64) o;
    else if ( o is Decimal ) return (int) (Decimal) o;
    else if ( o is Double ) return (int) (Double) o;
    else if ( o is UInt32 ) return (int) (UInt32) o;
    else if ( o is UInt64 ) return (int) (UInt64) o;
    else if ( o is Single ) return (int) (Single) o;

    throw new Exception( "CastToInt parametar nije broj." );
  }


  /// <summary>
  /// Konvertuje tip zadate vrednosti u tip Double,
  /// baca exception ako konverzija nije moguca.
  /// </summary>
  protected Double CastToDouble( Object o )
  {
    if ( o == null ) throw new Exception( "CastToDouble parametar je null." );

    if ( o is Double ) return (Double) o;
    else if ( o is Decimal ) return (Double) (Decimal) o;
    else if ( o is Single ) return (Double) (Single) o;
    else if ( o is int ) return (Double) (int) o;
    else if ( o is Int64 ) return (Double) (Int64) o;
    else if ( o is UInt32 ) return (Double) (UInt32) o;
    else if ( o is UInt64 ) return (Double) (UInt64) o;

    throw new Exception( "CastToDouble parametar nije broj." );
  }


  /// <summary>
  /// Vraca true ako je u toku eksplicitno pokrenuta transakcija.
  /// </summary>
  protected bool IsInTransaction
  {
    get
    {
      return ( m_state.Transaction != null );
    }
  }


  /// <summary>
  /// Zapocinje eksplicitno pokrenutu transakciju. Eksplicitno pokrenuta transakcija se mora zavrsiti
  /// sa metodom TransactionCommit ili TransactionRollback. Ako je transakcija vec u toku baca exception.
  /// Transakcija zapravo pocinje izvrsavanjem prve SQL komande posle TransactionStart, tj, ova metoda
  /// zapravo iskljucuje implicitno pozivanje TransactionCommit nakon svake pojedinacne SQL komande i
  /// time odlaze zatvaranje transakcije sve do prvog sledeceg TransactionCommit ili TransactionRollback.
  /// </summary>
  protected void TransactionStart()
  {
    if ( IsInTransaction )
    {
      throw new Exception( "TransactionStart() pozvan vise od jednom." );
    }

    m_state.Transaction = m_state.Connection.BeginTransaction();
  }

  /// <summary>
  /// Zavrsava eksplicitno pokrenutu transakciju, sve promene nad podacima uradjene u
  /// transakciji postaju zvanicne i vidljive ostalim korisnicima izvora podataka.
  /// Ako eksplicitna transakcija nije u toku, baca exception.
  /// </summary>
  protected void TransactionCommit()
  {
    if ( !IsInTransaction )
    {
      throw new Exception( "TransactionCommit() bez TransactionStart()." );
    }

    m_state.Transaction.Commit();
    m_state.Transaction.Dispose();
    m_state.Transaction = null;
  }


  /// <summary>
  /// Zavrsava eksplicitno pokrenutu transakciju, promene izvedene nad podacima u
  /// transakciji se ponistavaju. Moze se bezbedno pozvati i ako nije pokrenuta transakcija.
  /// Nikad ne baca exception.
  /// </summary>
  protected void TransactionRollback()
  {
    if ( IsInTransaction )
    {
      try
      {
        m_state.Transaction.Rollback();
      }
      catch
      {
      }

      try
      {
        m_state.Transaction.Dispose();
      }
      catch
      {
      }

      m_state.Transaction = null;
    }
  }

  /// <summary>
  /// Obradjuje zadati exception, izvrsava TransactionRollback nad tekucom
  /// transakcijuom, postavlja ErrorCode i ErrorMessage u zavisnosti od
  /// vrste i sadrzaja zadatog exception objekta. U slucaju da zadati
  /// exception predstavlja prekid veze sa MySQL serverom, pokusava da
  /// ponovo uspostavi vezu sa serverom i ako uspe metoda vraca true (kao 
  /// indikaciju da se cela transakcija u kojoj je ovaj exception nastupio
  /// ponovi). Takodje, u slucaju da zadati exception predstavlja MySQL
  /// gresku DEADLOCK, izvrsava TransactionRollback i metoda vraca true
  /// da se cela transakcija ponovi. Ova metoda je namenjena iskljucivo za
  /// upotrebu sa MySQL serverom i nece raditi ispravno sa drugim vrstama
  /// izvora podataka.
  /// </summary>
  /// <returns>Vraca true ako zadati exception moze biti prevazidjen
  /// ponovnim izvrsavanjem cele transakcije, inace, ako je greska
  /// nepopravljiva, vraca false.</returns>
  protected bool MySqlProcessException( Exception ex )
  {
    OdbcException odbc_ex = ex as OdbcException;

    if ( odbc_ex != null )
    {
      ErrorMessage = "";
      ErrorCode = -1;

      bool try_reconnect = false;
      bool syntax_error = false;

      foreach ( OdbcError error in odbc_ex.Errors )
      {
        if ( error.NativeError == 8701 ||       // lost connection
             error.NativeError == 2006 ||
             error.NativeError == 2013 )
        {
          try_reconnect = true;
          break;
        }
        else if ( error.NativeError == 1213 )   // deadlock
        {
          TransactionRollback();
          return true;
        }
        else if ( error.NativeError == 1064 )
        {
          syntax_error = true;
        }

        if ( ErrorCode == -1 ) ErrorCode = error.NativeError;
        if ( ErrorMessage.Length > 0 ) ErrorMessage += "\r\n";
        ErrorMessage += error.Message;
      }

      if ( syntax_error )
      {
        if ( ErrorMessage.Length > 0 ) ErrorMessage += "\r\n";
        ErrorMessage += "SQL: " + LastSqlCommand + "\r\n";
      }

      if ( try_reconnect )
      {
        String original_message = ErrorMessage;
        int original_code = ErrorCode;

        Disconnect();
        if ( Connect() ) return true;

        ErrorMessage = original_message;
        ErrorCode = original_code;
      }
      else
      {
        TransactionRollback();
      }
    }
    else
    {
      ErrorMessage = ex.ToString();
      ErrorCode = -1;
    }

    return false;
  }


  /// <summary>
  /// Izvrsava SQL izraz koji kao rezultat vraca tabelu,
  /// metoda vraca primljene tabelarne podatke kao objekat
  /// klase DataTable, ili baca exception u slucaju da
  /// nastupi greska. Metoda ce cekati podrazumevan broj
  /// sekundi (odredjen konstantom DefaultCommandTimeout)
  /// na pristizanje rezultata pre nego sto odustane i
  /// prijavi TIMEOUT gresku u vidu exception-a.
  /// </summary>
  protected DataTable ExecuteQuery( String sql_text )
  {
    return ExecuteQuery( sql_text, DefaultCommandTimeout );
  }


  /// <summary>
  /// Izvrsava SQL izraz koji kao rezultat vraca tabelu,
  /// metoda vraca primljene tabelarne podatke kao objekat
  /// klase DataTable, ili baca exception u slucaju da
  /// nastupi greska. Parametrom <paramref name="timeout"/>
  /// se zadaje vreme u sekundama koliko ce dugo metoda
  /// cekati na pristizanje rezultata pre nego sto odustane i
  /// prijavi TIMEOUT gresku u vidu exception-a.
  /// </summary>
  protected DataTable ExecuteQuery( String sql_text, int timeout )
  {
    LastSqlCommand = sql_text;

    OdbcCommand command = null;
    OdbcDataAdapter adapter = null;
    DataTable table = null;
    DataTable temp_table = null;

    try
    {
      command = new OdbcCommand();
      command.Connection = m_state.Connection;
      command.CommandText = sql_text;
      command.CommandTimeout = timeout;

      if ( IsInTransaction ) command.Transaction = m_state.Transaction;

      adapter = new OdbcDataAdapter( command );

      temp_table = new DataTable();
      adapter.Fill( temp_table );

      table = temp_table;
      temp_table = null;
    }
    finally
    {
      if ( temp_table != null ) temp_table.Dispose();
      if ( adapter != null ) adapter.Dispose();
      if ( command != null ) command.Dispose();
    }

    return table;
  }


  /// <summary>
  /// Izvrsava SQL izraz koji kao rezultat vraca broj
  /// slogova zahvacenih izvrsavanjem, sto ova metoda vraca
  /// kao povratnu vrednost. U slucaju greske baca exception.
  /// Metoda ce cekati podrazumevan broj sekundi (odredjen
  /// konstantom DefaultCommandTimeout) na zavrsetak
  /// izvrsavanja zadatog SQL izraza pre nego sto odustane
  /// i prijavi TIMEOUT gresku u vidu exception-a.
  /// </summary>
  protected int Execute( String sql_text )
  {
    CheckExpression( sql_text );
    return ExecuteUnsafe( sql_text, DefaultCommandTimeout );
  }


  /// <summary>
  /// Izvrsava SQL izraz koji kao rezultat vraca broj
  /// slogova zahvacenih izvrsavanjem, sto ova metoda vraca
  /// kao povratnu vrednost. U slucaju greske baca exception.
  /// Parametrom <paramref name="timeout"/> se zadaje vreme
  /// u sekundama koliko ce dugo metoda cekati na zavrsetak
  /// izvrsavanja zadatog SQL izraza pre nego sto odustane
  /// i prijavi TIMEOUT gresku u vidu exception-a.
  /// </summary>
  protected int Execute( String sql_text, int timeout )
  {
    CheckExpression( sql_text );
    return ExecuteUnsafe( sql_text, timeout );
  }


  /// <summary>
  /// Ova metoda radi isto kao i Execute sa tom razlikom da
  /// se ne radi provera da li zadati SQL izraz sadrzi UPDATE
  /// ili DELETE komandu bez zadatog WHILE. Izbegavati, moze
  /// da izazove gubitak podataka u slucaju programske greske.
  /// </summary>
  protected int ExecuteUnsafe( String sql_text )
  {
    return ExecuteUnsafe( sql_text, DefaultCommandTimeout );
  }


  /// <summary>
  /// Ova metoda radi isto kao i Execute sa tom razlikom da
  /// se ne radi provera da li zadati SQL izraz sadrzi UPDATE
  /// ili DELETE komandu bez zadatog WHILE. Izbegavati, moze
  /// da izazove gubitak podataka u slucaju programske greske.
  /// </summary>
  protected int ExecuteUnsafe( String sql_text, int timeout )
  {
    LastSqlCommand = sql_text;

    OdbcCommand command = null;

    int result = -1;

    try
    {
      command = new OdbcCommand();
      command.Connection = m_state.Connection;
      command.CommandText = sql_text;
      command.CommandTimeout = timeout;

      if ( IsInTransaction ) command.Transaction = m_state.Transaction;

      result = command.ExecuteNonQuery();
    }
    finally
    {
      if ( command != null ) command.Dispose();
    }

    return result;
  }


  /// <summary>
  /// Izvrsava SQL izraz koji kao rezultat vraca tabelu,
  /// metoda vraca samo vrednost upisanu u prvu kolonu prvog sloga,
  /// vraca null ako rezultat nema ni jedan slog, ili baca exception u
  /// slucaju da nastupi greska. Metoda ce cekati podrazumevan broj
  /// sekundi (odredjen konstantom DefaultCommandTimeout)
  /// na pristizanje rezultata pre nego sto odustane i
  /// prijavi TIMEOUT gresku u vidu exception-a.
  /// </summary>
  protected Object ExecuteScalar( String sql_text )
  {
    return ExecuteScalar( sql_text, DefaultCommandTimeout );
  }


  /// <summary>
  /// Izvrsava SQL izraz koji kao rezultat vraca tabelu,
  /// metoda vraca samo vrednost upisanu u prvu kolonu prvog sloga,
  /// vraca null ako rezultat nema ni jedan slog, ili baca exception u
  /// slucaju da nastupi greska. Parametrom <paramref name="timeout"/>
  /// se zadaje vreme u sekundama koliko ce dugo metoda
  /// cekati na pristizanje rezultata pre nego sto odustane i
  /// prijavi TIMEOUT gresku u vidu exception-a.
  /// </summary>
  protected Object ExecuteScalar( String sql_text, int timeout )
  {
    LastSqlCommand = sql_text;

    OdbcCommand command = null;

    Object result = null;

    try
    {
      command = new OdbcCommand();
      command.Connection = m_state.Connection;
      command.CommandText = sql_text;
      command.CommandTimeout = timeout;

      if ( IsInTransaction ) command.Transaction = m_state.Transaction;

      result = command.ExecuteScalar();
    }
    finally
    {
      if ( command != null ) command.Dispose();
    }

    return result;
  }


  /// <summary>
  /// Priprema SQL izraz za visestruko izvrsavanje, ovim se
  /// izbegava potreba za kompajliranjem SQL izraza pre
  /// svakog izvrsavanja i time se postize brze izvrsavanje
  /// istog izraza vise puta. Vraca objekat OdbcCommand klase
  /// koji se dalje upotrebljava sa metodom Execute koji prima
  /// ovaj objekat kao prvi parametar. U SQL izrazu se na 
  /// specificnim mestima mogu postaviti parametri SQL izraza
  /// koriscenjem znaka ?, pri izvrsavanju na tim mestima se
  /// ubacuju konkretne vrednosti koje se zadaju pri svakom 
  /// pojedinacnom pozivu metode Execute. Metoda podesava
  /// OdbcCommand objekat tako da, kad bude izvrsavan, ceka
  /// svaki put podrazumevan broj sekundi (odredjen konstantom
  /// DefaultCommandTimeout) na zavrsetak izvrsavanja zadatog
  /// SQL izraza pre nego sto odustane i prijavi TIMEOUT gresku
  /// u vidu exception-a (ova strategija cekanja, dakle, pocinje
  /// da se primenjuje tek kad se OdbcCommand objekat upotrebi
  /// u Execute metodi). Prepare metoda moze da baci exception
  /// pod odredjenim uslovima tako da bi trebalo da bude
  /// zasticena try-catch blokom. Vraceni OdbcCommand objekat
  /// je vezan za tekucu konekciju i gubi se ukoliko se veza
  /// sa izvorom podataka prekine i uspostavi ponovo.
  /// OdbcCommand objekat bi nakon upotrebe trebalo osloboditi
  /// pozivanjem njegove metode Dispose.
  /// </summary>
  protected OdbcCommand Prepare( String sql_text )
  {
    CheckExpression( sql_text );
    return PrepareUnsafe( sql_text, DefaultCommandTimeout );
  }


  /// <summary>
  /// Priprema SQL izraz za visestruko izvrsavanje, ovim se
  /// izbegava potreba za kompajliranjem SQL izraza pre
  /// svakog izvrsavanja i time se postize brze izvrsavanje
  /// istog izraza vise puta. Vraca objekat OdbcCommand klase
  /// koji se dalje upotrebljava sa metodom Execute koji prima
  /// ovaj objekat kao prvi parametar. U SQL izrazu se na 
  /// specificnim mestima mogu postaviti parametri SQL izraza
  /// koriscenjem znaka ?, pri izvrsavanju na tim mestima se
  /// ubacuju konkretne vrednosti koje se zadaju pri svakom 
  /// pojedinacnom pozivu metode Execute.
  /// Parametar <paramref name="timeout"/> predstavlja vreme u
  /// sekundama sa kojim ce OdbcCommand objekat biti podesen
  /// tako da, kad bude izvrsavan, toliko dugo ceka svaki put
  /// na zavrsetak izvrsavanja zadatog SQL izraza pre nego sto
  /// odustane i prijavi TIMEOUT gresku u vidu exception-a
  /// (ova strategija cekanja, dakle, pocinje da se primenjuje
  /// tek kad se OdbcCommand objekat upotrebi u Execute metodi).
  /// Ova metoda moze da baci exception pod odredjenim uslovima
  /// tako da bi trebalo da bude zasticena try-catch blokom.
  /// Vraceni OdbcCommand objekat je vezan za tekucu konekciju
  /// i gubi se ukoliko se veza sa izvorom podataka izgubi i
  /// uspostavi ponovo. OdbcCommand objekat bi nakon upotrebe
  /// trebalo osloboditi pozivanjem njegove metode Dispose.
  /// </summary>
  protected OdbcCommand Prepare( String sql_text, int timeout )
  {
    CheckExpression( sql_text );
    return PrepareUnsafe( sql_text, timeout );
  }


  /// <summary>
  /// Ova metoda radi isto kao i Prepare sa tom razlikom da
  /// se ne radi provera da li zadati SQL izraz sadrzi UPDATE
  /// ili DELETE komandu bez zadatog WHILE. Izbegavati, moze
  /// da izazove gubitak podataka u slucaju programske greske.
  /// </summary>
  protected OdbcCommand PrepareUnsafe( String sql_text )
  {
    return PrepareUnsafe( sql_text, DefaultCommandTimeout );
  }


  /// <summary>
  /// Ova metoda radi isto kao i Prepare sa tom razlikom da
  /// se ne radi provera da li zadati SQL izraz sadrzi UPDATE
  /// ili DELETE komandu bez zadatog WHILE. Izbegavati, moze
  /// da izazove gubitak podataka u slucaju programske greske.
  /// </summary>
  protected OdbcCommand PrepareUnsafe( String sql_text, int timeout )
  {
    LastSqlCommand = sql_text;

    OdbcCommand result = null;
    OdbcCommand command = null;

    try
    {
      command = new OdbcCommand();
      command.Connection = m_state.Connection;
      command.CommandText = sql_text;
      command.CommandTimeout = timeout;
      if ( IsInTransaction ) command.Transaction = m_state.Transaction;

      command.Prepare();

      result = command;
      command = null;
    }
    finally
    {
      if ( command != null ) command.Dispose();
    }

    return result;
  }


  /// <summary>
  /// Izvrsava prethodno pripremljen SQL izraz (pripremljen sa metodom Prepare).
  /// Pripremljen SQL izraz treba da vraca broj slogova zahvacenih izvrsavanjem
  /// sto ova metoda vraca kao povratnu vrednost. U slucaju greske baca exception.
  /// Parametri SQL izraza navedeni postavljanjem znakova ? ce biti zamenjeni,
  /// po redosledu pojavljivanja u SQL izrazu, sa vrednostima koje budu navedene
  /// redom u parametru <paramref name="parameters"/>. 
  /// </summary>
  protected int Execute( OdbcCommand command,
                         params Object[] parameters )
  {
    if ( parameters.Length > command.Parameters.Count )
      for ( int i=command.Parameters.Count ; i<parameters.Length ; i++ )
        command.Parameters.Add( command.CreateParameter() );

    for ( int i=0 ; i<parameters.Length ; i++ )
      command.Parameters[ i ].Value = parameters[ i ];

    return command.ExecuteNonQuery();
  }



  void CheckExpression( String sql_text )
  {
    String sql_text_uc = sql_text.Trim().ToUpperInvariant();

    if ( !sql_text_uc.Contains( "WHERE" ) &&
         ( sql_text_uc.StartsWith( "UPDATE" ) ||
           sql_text_uc.StartsWith( "DELETE" ) ) )
    {
      throw new Exception( "Upotrebljen UPDATE ili DELETE bez WHERE.\r\n\r\n" +
                           "SQL izraz:\r\n" + sql_text );
    }
  }

}
