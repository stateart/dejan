using System;
using System.Data;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

public class Browse
{
  public Browse( DataGridView dgv,
                 Label filter,
                 DataTable tabela )
  {
    if ( tabela == null ) throw new Exception( "new Browse(), tabela je null." );
    if ( tabela.Columns.Count == 0 ) throw new Exception( "new Browse(), tabela nema ni jednu kolonu." );

    if ( tabela.PrimaryKey == null || tabela.PrimaryKey.Length == 0 )
    {
      throw new Exception( "new Browse(), tabela nema prijavljen primarni kljuc." );
    }

    setupZavrsen = false;

    statickiFilter = "";
    SirinaUJedinicamaM = false;
    sirinaM = 0.0;

    filterTimer = new Timer();
    filterTimer.Interval = 500;
    filterTimer.Tick += tick;

    defineColList = new List<Kolona>();
    filterColList = new List<int>();

    this.table = tabela;
    view = null;

    this.dgv = dgv;
    this.filter = filter;

    dgvTypicalSetup( dgv );

    refreshFirstVisibleCol = -1;
    refreshFirstVisibleRow = -1;
    refreshViewRow = -1;
    refreshPrimaryKey = null;

    cellFormattingBrojac = 0;

    rowPreStyleFunkcija = null;
    rowPostStyleFunkcija = null;
  }



  public String StatickiFilter
  {
    get
    {
      return statickiFilter;
    }

    set
    {
      if ( String.IsNullOrEmpty( value ) )
      {
        statickiFilter = "";
      }
      else
      {
        statickiFilter = value.Trim();
      }

      if ( setupZavrsen ) uspostaviFilter();
    }
  }



  public Action<DataGridViewCellStyle, DataRow> RowPreStyleFunkcija
  {
    get { return rowPreStyleFunkcija; }

    set
    {
      if ( value == null )
      {
        cellFormattingBrojac--;
        if ( cellFormattingBrojac == 0 ) dgv.CellFormatting -= cellFormatting;
      }
      else
      {
        if ( cellFormattingBrojac == 0 )
        {
          dgv.CellFormatting -= cellFormatting;
          dgv.CellFormatting += cellFormatting;
        }

        cellFormattingBrojac++;
      }

      rowPreStyleFunkcija = value;
    }
  }



  public Action<DataGridViewCellStyle, DataRow> RowPostStyleFunkcija
  {
    get { return rowPostStyleFunkcija; }

    set
    {
      if ( value == null )
      {
        cellFormattingBrojac--;
        if ( cellFormattingBrojac == 0 ) dgv.CellFormatting -= cellFormatting;
      }
      else
      {
        if ( cellFormattingBrojac == 0 )
        {
          dgv.CellFormatting -= cellFormatting;
          dgv.CellFormatting += cellFormatting;
        }

        cellFormattingBrojac++;
      }

      rowPostStyleFunkcija = value;
    }
  }


  public bool SirinaUJedinicamaM { get; set; }


  public void DodajKolonu( String name,
                           String headerText = null,
                           String format = null,
                           String sortAsc = null,
                           String sortDesc = null,
                           Double width = 0,
                           DataGridViewContentAlignment align =
                             DataGridViewContentAlignment.NotSet,
                           Func<Object,String> formatFunkcija = null,
                           Action<DataGridViewCellStyle,Object> cellStyleFunkcija = null )
  {
    if ( setupZavrsen ) throw new Exception( "Browse.DodajKolonu() pozvana posle Browse.SetupZavrsen()." );

    if ( String.IsNullOrEmpty( name ) ) throw new Exception( "Browse.DodajKolonu(), ime kolone mora biti navedeno." );

    name = name.Trim().ToUpper();

    foreach ( Kolona kol in defineColList )
      if ( kol.name == name )
        throw new Exception( "Browse.DodajKolonu() kolona sa imenom \"" + name + "\" je vec dodata." );

    DataColumn column = table.Columns[ name ];
    if ( column == null ) throw new Exception( "Browse.DodajKolonu(), kolona sa imenom \"" + name + "\" nije nadjena u DataTable objektu." );

    Kolona kolona = new Kolona();

    kolona.name = name;
    kolona.tip = TipKolone.Normalna;
    kolona.tableColumnPos = column.Ordinal;
    kolona.dgvColumnPos = -1;
    kolona.headerText = headerText ?? name;
    kolona.format = format;
    kolona.formatFunkcija = formatFunkcija;
    kolona.sortAsc = sortAsc ?? name + " asc";
    kolona.sortDesc = sortDesc ?? name + " desc";

    int col_pos = kolona.tableColumnPos;
    kolona.getCellValue = ( dgv_row ) => view[ dgv_row ][ col_pos ];

    if ( align == DataGridViewContentAlignment.NotSet )
    {
      if ( column.DataType == typeof(Int32) ||
           column.DataType == typeof(Double) ||
           column.DataType == typeof(Decimal) ||
           column.DataType == typeof(Int64) ||
           column.DataType == typeof(Single) ||
           column.DataType == typeof(Int16) ||
           column.DataType == typeof(SByte) ||
           column.DataType == typeof(Byte) ||
           column.DataType == typeof(UInt16) ||
           column.DataType == typeof(UInt32) ||
           column.DataType == typeof(UInt64) )
      {
        kolona.align = DataGridViewContentAlignment.MiddleRight;
      }
      else
      {
        kolona.align = DataGridViewContentAlignment.MiddleLeft;
      }
    }
    else
    {
      kolona.align = align;
    }

    if ( SirinaUJedinicamaM && sirinaM <= 0.0 )
    {
      sirinaM = TextRenderer.MeasureText( new String( 'M', 100 ), dgv.Font ).Width / 100.0;
    }

    if ( width <= 0.0 )
    {
      if ( SirinaUJedinicamaM ) width = 10.0;
                           else width = 100.0;
    }

    if ( SirinaUJedinicamaM ) kolona.width = (int) ( width * sirinaM + 0.5 );
                         else kolona.width = (int) ( width + 0.5 );

    kolona.cellStyleFunkcija = cellStyleFunkcija;

    defineColList.Add( kolona );
  }



  public void DodajFilter( String imeCol )
  {
    if ( setupZavrsen ) throw new Exception( "Browse.DodajFilter() pozvana posle Browse.SetupZavrsen()." );

    DataColumn column = table.Columns[ imeCol ];
    if ( column == null ) throw new Exception( "Browse.DodajFilter() tabela ne sadrzi kolonu sa imenom \"" + imeCol + "\"." );

    filterColList.Add( column.Ordinal );
  }



  public void SetupZavrsen()
  {
    if ( setupZavrsen ) throw new Exception( "Browse.SetupZavrsen() pozvana vise puta." );

    dgvColumnSetup();

    setupZavrsen = true;

    view = new DataView( table );

    dgv.CellValueNeeded += cellValueNeeded;
    dgv.ColumnHeaderMouseClick += columnHeaderClick;
    dgv.CellMouseClick += cellMouseEvent;
    dgv.CellMouseDoubleClick += cellMouseEvent;
    dgv.KeyDown += keyDown;
    dgv.KeyPress += keyPress;

    dgv.VirtualMode = true;

    uspostaviFilter();

    dgvSetFontSize( dgv,
                    dgv.Font.FontFamily.Name,
                    dgv.Font.SizeInPoints );

    resetRowCount( view.Count );
  }



  public void ZamrzniDoKolone(Int32 frozenDoKolone)
  {
     if ( frozenDoKolone >= 0 )  // -1 ne zamrzava
     {
       if ( !setupZavrsen ) throw new Exception( "Browse.ZamrzniDoKolone() pozvana pre Browse.SetupZavrsen().");
       if ( frozenDoKolone > dgv.Columns.Count - 1 ) throw new Exception("Prevelik broj u Browse.ZamrzniDoKolone(): " + frozenDoKolone.ToString());
       else dgv.Columns[ frozenDoKolone ].Frozen = true;
     }
  }



  public Action<KeyEventArgs> TasterAkcija;
  public Action<DataGridViewCellMouseEventArgs> MouseAkcija;



  public void IzmenePocetak()
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.IzmenePocetak() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    refreshFirstVisibleCol = dgv.FirstDisplayedScrollingColumnIndex;
    refreshFirstVisibleRow = dgv.FirstDisplayedScrollingRowIndex;
    refreshViewRow = dgv.CurrentCellAddress.Y;
    refreshTableRow = viewRowToTableRow( refreshViewRow );
    refreshPrimaryKey = viewRowToPrimaryKey( refreshViewRow );
  }



  public void IzmeneZavrsene()
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.IzmeneZavrsene() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    izmeneZavrsene( primaryKeyToViewRow( refreshPrimaryKey ) );
  }



  public void IzmeneZavrseneSelektujRed( int red )
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.IzmeneZavrsene() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    izmeneZavrsene( tableRowToViewRow( red ) );
  }



  public void IzmeneZavrseneSelektujPrimarniKljuc( Object[] primarniKljuc )
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.IzmeneZavrsene() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    izmeneZavrsene( primaryKeyToViewRow( primarniKljuc ) );
  }



  public void IzmeneZavrseneSelektujId( int id )
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.IzmeneZavrsene() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    int viewRow = -1;

    if ( id > 0 ) viewRow = primaryKeyToViewRow( new Object[] { id } );

    izmeneZavrsene( viewRow );
  }



  public void SelektujRed( int row )
  {
    if ( !setupZavrsen )
      throw new Exception( "Browse.SelektujRed() pozvana pre Browse.SetupZavrsen()." );

    if ( table == null || view == null || dgv == null ) return;

    int viewRow = tableRowToViewRow( row );

    if ( viewRow >= 0 )
    {
      dgv[ 0, viewRow ].Selected = true;
      dgv.CurrentCell = dgv[ 0, viewRow ];
    }
    else
    {
      SelektujPrvi();
    }
  }



  public void SelektujPrimarniKljuc( Object[] primarniKljuc )
  {
    if ( !setupZavrsen )
      throw new Exception( "Browse.SelektujPrimarniKljuc() pozvana pre Browse.SetupZavrsen()." );

    if ( table == null || view == null || dgv == null ) return;

    int viewRow = primaryKeyToViewRow( primarniKljuc );

    if ( viewRow >= 0 )
    {
      dgv[ 0, viewRow ].Selected = true;
      dgv.CurrentCell = dgv[ 0, viewRow ];
    }
    else
    {
      SelektujPrvi();
    }
  }



  public void SelektujId( int id )
  {
    if ( !setupZavrsen )
      throw new Exception( "Browse.SelektujId() pozvana pre Browse.SetupZavrsen()." );

    if ( table == null || view == null || dgv == null ) return;

    int viewRow = -1;
    
    if ( id > 0 ) viewRow = primaryKeyToViewRow( new Object[] { id } );

    if ( viewRow >= 0 )
    {
      dgv[ 0, viewRow ].Selected = true;
      dgv.CurrentCell = dgv[ 0, viewRow ];
    }
    else
    {
      SelektujPrvi();
    }
  }



  public void SelektujPrvi()
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.SelektujPrvi() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    if ( dgv.RowCount > 0 )
    {
      dgv[ 0, 0 ].Selected = true;
      dgv.CurrentCell = dgv[ 0, 0 ];
    }
  }



  public void SelektujZadnji()
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.SelektujZadnji() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    if ( dgv.RowCount > 0 )
    {
      dgv[ 0, dgv.RowCount - 1 ].Selected = true;
      dgv.CurrentCell = dgv[ 0, dgv.RowCount - 1 ];
    }
  }



  public int SelektovaniRed
  {
    get
    {
      return viewRowToTableRow( dgv.CurrentCellAddress.Y );
    }

    set
    {
      SelektujRed( value );
    }
  }



  public Object[] SelektovaniPrimarniKljuc
  {
    get
    {
      return viewRowToPrimaryKey( dgv.CurrentCellAddress.Y );
    }

    set
    {
      SelektujPrimarniKljuc( value );
    }
  }



  public int SelektovaniId
  {
    get
    {
      Object[] primarniKljuc = viewRowToPrimaryKey( dgv.CurrentCellAddress.Y );

      if ( primarniKljuc == null ) return -1;
      if ( primarniKljuc.Length == 0 ) return -1;

      return (int) primarniKljuc[ 0 ]; 
    }

    set
    {
      SelektujId( value );
    }
  }



  public void UspostaviSort( String imeKolone,
                             SortOrder redosled = SortOrder.None )
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.UspostaviSort() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    DataGridViewColumn dgvKolona = dgv.Columns[ imeKolone ];
    if ( dgvKolona == null ) return;

    uspostaviSort( dgvKolona.Index, redosled );
  }



  public void UspostaviSort( int brojKolone,
                             SortOrder redosled = SortOrder.None )
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.UspostaviSort() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    uspostaviSort( brojKolone, redosled );
  }



  public void UspostaviNezavisniSort( String sort_izraz )
  {
    if ( !setupZavrsen ) throw new Exception( "Browse.UspostaviNezavisniSort() pozvana pre Browse.SetupZavrsen()." );
    if ( table == null || view == null || dgv == null ) return;

    foreach ( DataGridViewColumn col in dgv.Columns )
    {
      col.HeaderCell.SortGlyphDirection = SortOrder.None;
    }

    IzmenePocetak();

    resetRowCount();
    view.Sort = sort_izraz;

    IzmeneZavrsene();
  }



  public void ObustaviIzmenuFiltera()
  {
    if ( filter == null || filterTimer == null ) return;

    filterTimer.Stop();

    if ( filterStari != null )
    {
      filter.Text = filterStari;
      filterStari = null;
    }
  }



  public void RotirajSort()
  {
    foreach ( DataGridViewColumn kol in dgv.Columns )
    {
      if ( kol.HeaderCell.SortGlyphDirection != SortOrder.None )
      {
        int index = kol.Index;

        for (;;)
        {
          index++;
          if ( index >= dgv.Columns.Count ) index = 0;
          if ( index == kol.Index ) break;

          Kolona kolona = dgv.Columns[ index ].Tag as Kolona;

          if ( kolona != null )
          {
            if ( !String.IsNullOrEmpty( kolona.sortAsc ) )
            {
              uspostaviSort( index, SortOrder.Ascending );
              break;
            }
          }
        }

        break;
      }
    }
  }



  public void ZameniTabelu( DataTable tabela )
  {
    IzmenePocetak();

    resetRowCount();

    String viewSort = view.Sort;
    String viewFilter = view.RowFilter;

    view.Dispose();

    if ( tabela.PrimaryKey == null || tabela.PrimaryKey.Length == 0 )
    {
      DataColumn[] primaryKey = new DataColumn[ table.PrimaryKey.Length ];

      for ( int i = 0 ; i < table.PrimaryKey.Length ; i++ )
      {
        DataColumn kolona = tabela.Columns[ table.PrimaryKey[ i ].ColumnName ];
        if ( kolona == null ) throw new Exception( "Browse.ZameniTabelu tabele se previse razlikuju." );

        primaryKey[ i ] = kolona;
      }

      tabela.PrimaryKey = primaryKey;
    }

    table = tabela;

    view = new DataView( table );

    view.Sort = viewSort;
    view.RowFilter = viewFilter;
    
    IzmeneZavrsene();
  }



  public void Ukloni()
  {
    // prvo otkaci dogadjaje
    if ( dgv != null )
    {
      resetRowCount();
      dgv.CellValueNeeded -= cellValueNeeded;
      dgv.CellFormatting -= cellFormatting;
      dgv.ColumnHeaderMouseClick -= columnHeaderClick;
      dgv.CellMouseClick -= cellMouseEvent;
      dgv.CellMouseDoubleClick -= cellMouseEvent;
      dgv.KeyDown -= keyDown;
      dgv.KeyPress -= keyPress;
    }

    if ( filterTimer != null )
    {
      filterTimer.Stop();
      filterTimer.Tick -= tick;
      filterTimer.Dispose();
      filterTimer = null;
    }

    if ( view != null )
    {
      view.Dispose();
      view = null;
    }

    if ( table != null )
    {
      table = null;
    }

    filter = null;
    dgv = null;
  }



  // samo privatni clanovi klase ispod ove linije

  DataTable table;
  DataView view;
  DataGridView dgv;
  Label filter;
  List<Kolona> defineColList;
  List<int> filterColList;
  String statickiFilter;
  Timer filterTimer;
  String filterStari;
  bool setupZavrsen;
  int refreshFirstVisibleRow;
  int refreshFirstVisibleCol;
  int refreshViewRow;
  int refreshTableRow;
  Object[] refreshPrimaryKey;
  Double sirinaM;
  int cellFormattingBrojac;
  Action<DataGridViewCellStyle, DataRow> rowPreStyleFunkcija;
  Action<DataGridViewCellStyle, DataRow> rowPostStyleFunkcija;


  class Kolona
  {
    public String name;
    public TipKolone tip;
    public int tableColumnPos;
    public int dgvColumnPos;
    public String headerText;
    public String format;
    public String sortAsc;
    public String sortDesc;
    public DataGridViewContentAlignment align;
    public int width;
    public Func<int,Object> getCellValue;
    public Func<Object,String> formatFunkcija;
    public Action<DataGridViewCellStyle, Object> cellStyleFunkcija;
  }



  enum TipKolone
  {
    Normalna
  }



  void dgvTypicalSetup( DataGridView dgv )
  {
     dgv.AllowUserToAddRows = false;      // dodavanje redova
     dgv.AllowUserToDeleteRows = false;   // brisanje redova
     dgv.AllowUserToResizeRows = false;   // prosirivanje redova
     dgv.AllowUserToOrderColumns = false; //� true � promene redosleda kolone
     dgv.AutoSize = false; // promena velicine celije da stane podatak
     dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None; // ne menja sirinu redova
     dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None; // ne menja sirinu kolone
     dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing; // promena visine zaglavlja
     dgv.EditMode = DataGridViewEditMode.EditProgrammatically; // BEZ DIREKTNIH izmena na browsu [ ili BeginEdit(false) ]
     dgv.MultiSelect = false;  // istovremena selekcija vise redova/kolona
     dgv.RowHeadersVisible = false; // bez predkolone redova
     dgv.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing; // promena visine kolona
     dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect; // svetle sve kolone u redu
  }



  void resetRowCount()
  {
    dgv.CellValueNeeded -= cellValueNeeded;
    dgv.CellFormatting -= cellFormatting;
    dgv.RowCount = 0;
  }



  void resetRowCount( int rowCount )
  {
    dgv.CellValueNeeded -= cellValueNeeded;
    dgv.CellFormatting -= cellFormatting;
    dgv.RowCount = 0;

    dgv.CellValueNeeded += cellValueNeeded;
    if ( cellFormattingBrojac > 0 ) dgv.CellFormatting += cellFormatting;
    dgv.RowCount = rowCount;
  }



  Object[] getPrimaryKey( DataRow row )
  {
    if ( row == null ) return null;
    if ( table.PrimaryKey == null ) return null;
    if ( table.PrimaryKey.Length == 0 ) return null;

    Object[] result = new Object[ table.PrimaryKey.Length ];

    for ( int i = 0 ; i < table.PrimaryKey.Length ; i++ )
      result[ i ] = row[ table.PrimaryKey[ i ] ];

    return result;
  }



  int tableRowToViewRow( int tableRow )
  {
    if ( tableRow < 0 || tableRow >= table.Rows.Count ) return -1;

    DataRow data_row = table.Rows[ tableRow ];

    for ( int i = 0 ; i < view.Count ; i++ )
      if ( ReferenceEquals( view[ i ].Row, data_row ) )
        return i;

    return -1;
  }



  int viewRowToTableRow( int viewRow )
  {
    if ( viewRow < 0 || viewRow >= view.Count ) return -1;
    return table.Rows.IndexOf( view[ viewRow ].Row );
  }



  Object[] tableRowToPrimaryKey( int tableRow )
  {
    if ( tableRow < 0 || tableRow >= table.Rows.Count ) return null;
    return getPrimaryKey( table.Rows[ tableRow ] );
  }



  Object[] viewRowToPrimaryKey( int viewRow )
  {
    if ( viewRow < 0 || viewRow >= view.Count ) return null;
    return getPrimaryKey( view[ viewRow ].Row );
  }



  int primaryKeyToTableRow( Object[] primaryKey )
  {
    if ( primaryKey == null ) return -1;

    DataRow dataRow = table.Rows.Find( primaryKey );
    if ( dataRow == null ) return -1;

    return table.Rows.IndexOf( dataRow );
  }



  int primaryKeyToViewRow( Object[] primaryKey )
  {
    return tableRowToViewRow( primaryKeyToTableRow( primaryKey ) );
  }



  void dgvColumnSetup()
  {
    if ( defineColList.Count == 0 )
    {
      for ( int i=0 ; i<table.Columns.Count ; i++ )
        DodajKolonu( table.Columns[ i ].ColumnName );
    }

    dgv.Columns.Clear();

    for ( int i=0 ; i<defineColList.Count ; i++ )
    {
      Kolona kolona = defineColList[ i ];

      DataGridViewColumn column = null;

      switch ( kolona.tip )
      {
        case TipKolone.Normalna:
          column = setupNormalneKolone( kolona );
          break;

        default:
          throw new Exception( "Browse.dgvColumnSetup() nepoznat tip kolone." );
      }

      if ( kolona.formatFunkcija != null ||
           kolona.cellStyleFunkcija != null )
      {
        if ( cellFormattingBrojac == 0 )
        {
          dgv.CellFormatting -= cellFormatting;
          dgv.CellFormatting += cellFormatting;
        }

        cellFormattingBrojac++;
      }

      dgv.Columns.Add( column );
    }
  }



  DataGridViewColumn setupNormalneKolone( Kolona kolona )
  {
    DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();

    column.Tag = kolona;
    column.Name = kolona.name;
    column.HeaderText = kolona.headerText;

    if ( kolona.sortAsc == "" ) column.SortMode = DataGridViewColumnSortMode.NotSortable;
                           else column.SortMode = DataGridViewColumnSortMode.Programmatic;

    if ( !String.IsNullOrEmpty( kolona.format ) )
    {
      column.DefaultCellStyle.Format = kolona.format;
    }

    column.DefaultCellStyle.Alignment = kolona.align;

    column.Width = kolona.width;

    return column;
  }



  void izmeneZavrsene( int viewRow )
  {
    resetRowCount( view.Count );

    if ( refreshFirstVisibleCol >= 0 &&
         refreshFirstVisibleCol < view.Table.Columns.Count )
    {
      dgv.FirstDisplayedScrollingColumnIndex = refreshFirstVisibleCol;
    }

    if ( view.Count > 0 )
    {
      if ( refreshFirstVisibleRow >= 0 && refreshFirstVisibleRow < view.Count )
      {
        dgv.FirstDisplayedScrollingRowIndex = refreshFirstVisibleRow;
      }

      if ( viewRow < 0 )
      {
        viewRow = refreshViewRow;

        if ( viewRow >= view.Count ) viewRow = view.Count - 1;
        if ( viewRow < 0 ) viewRow = 0;
      }

      dgv[ 0, viewRow ].Selected = true;
      dgv.CurrentCell = dgv[ 0, viewRow ];
    }

    refreshFirstVisibleCol = -1;
    refreshFirstVisibleRow = -1;
    refreshViewRow = -1;
    refreshPrimaryKey = null;
  }



  void uspostaviSort( int brojKolone,
                      SortOrder redosled )
  {
    Kolona kolona = defineColList[ brojKolone ];
    if ( String.IsNullOrEmpty( kolona.sortAsc ) ) return;

    DataGridViewColumn dgvKolona = dgv.Columns[ brojKolone ];

    SortOrder tekuciSort = dgvKolona.HeaderCell.SortGlyphDirection;

    if ( redosled != SortOrder.None && tekuciSort == redosled ) return;

    foreach ( DataGridViewColumn kol in dgv.Columns )
    {
      kol.HeaderCell.SortGlyphDirection = SortOrder.None;
    }

    IzmenePocetak();

    if ( redosled == SortOrder.Ascending ||
         ( redosled == SortOrder.None && tekuciSort != SortOrder.Ascending ) )
    {
      resetRowCount();
      view.Sort = kolona.sortAsc;
      dgvKolona.HeaderCell.SortGlyphDirection = SortOrder.Ascending;
      IzmeneZavrsene();
    }
    else if ( !String.IsNullOrEmpty( kolona.sortDesc ) )
    {
      resetRowCount();
      view.Sort = kolona.sortDesc;
      dgvKolona.HeaderCell.SortGlyphDirection = SortOrder.Descending;
      IzmeneZavrsene();
    }
  }



  void uspostaviFilter()
  {
    filterTimer.Stop();
    filterStari = null;

    String izraz = formirajFilterskiIzraz( filter.Text );

    IzmenePocetak();

    resetRowCount();
    view.RowFilter = izraz;

    IzmeneZavrsene();

    filter.ResetForeColor();
  }



  String formirajFilterskiIzraz( String text )
  {
    String izraz = "";

    text = text.Trim();

    if ( text.Length > 0 && filterColList.Count > 0 )
    {
      String[] reci = text.Split( new Char[] { ' ', '\t' },
                                  StringSplitOptions.RemoveEmptyEntries );

      List<String> likeStrings = new List<String>( reci.Length );

      foreach ( String rec in reci ) likeStrings.Add( likeString( rec ) );

      bool prvoPolje = true;

      foreach ( int polje in filterColList )
      {
        if ( prvoPolje ) prvoPolje = false;
                     else izraz += " OR ";

        DataColumn column = table.Columns[ polje ];

        String sqlPolje;

        if ( column.DataType == typeof(String) )
        {
          sqlPolje = "`" + column.ColumnName + "`";
        }
        else
        {
          sqlPolje = "convert(`" + column.ColumnName + "`,'System.String')";
        }

        bool prvaRec = true;

        izraz += "(";

        for ( int i=0 ; i<likeStrings.Count ; i++ )
        {
          if ( prvaRec ) prvaRec = false;
                    else izraz += " AND ";

          izraz += sqlPolje + " LIKE '%" + likeStrings[ i ] + "%'";
        }

        izraz += ")";
      }
    }

    if ( !String.IsNullOrEmpty( statickiFilter ) )
    {
      if ( izraz.Length > 0 ) izraz = statickiFilter + " AND (" + izraz + ")";
                         else izraz = statickiFilter;
    }

    return izraz;
  }



  String likeString( String text )
  {
    StringBuilder new_text = new StringBuilder( text.Length * 3 );

    for ( int i=0 ; i<text.Length ; i++ )
    {
      Char ch = text[ i ];

      switch ( ch )
      {
        case '[':

          new_text.Append( "[[]" );
          break;

        case ']':

          new_text.Append( "[]]" );
          break;

        case '*':

          new_text.Append( "[*]" );
          break;

        case '%':

          new_text.Append( "[%]" );
          break;

        case '\'':

          new_text.Append( "''" );
          break;

        default:

          new_text.Append( ch );
          break;
      }
    }

    return new_text.ToString();
  }



  void cellValueNeeded( Object sender, DataGridViewCellValueEventArgs e )
  {
    e.Value = defineColList[ e.ColumnIndex ].getCellValue( e.RowIndex );
  }



  void cellFormatting( Object sender, DataGridViewCellFormattingEventArgs e )
  {
    if ( e.RowIndex >= 0 && e.RowIndex < view.Count )
    {
      if ( rowPreStyleFunkcija != null )
      {
        rowPreStyleFunkcija( e.CellStyle, view[ e.RowIndex ].Row );
      }
      
      Kolona kolona = defineColList[ e.ColumnIndex ];

      if ( kolona.cellStyleFunkcija != null )
      {
        kolona.cellStyleFunkcija( e.CellStyle, e.Value );
      }

      if ( kolona.formatFunkcija != null )
      {
        e.Value = kolona.formatFunkcija( e.Value );
        e.FormattingApplied = true;
      }
      
      if ( rowPostStyleFunkcija != null )
      {
        rowPostStyleFunkcija( e.CellStyle, view[ e.RowIndex ].Row );
      }
    }
  }



  void columnHeaderClick( Object sender, DataGridViewCellMouseEventArgs e )
  {
    if ( view == null ) return;
    uspostaviSort( e.ColumnIndex, SortOrder.None );
  }



  void keyPress( Object sender, KeyPressEventArgs e )
  {
    if ( view == null || filterTimer == null ) return;

    String stari = filter.Text;
    bool promenjen = false;

    if ( e.KeyChar >= ' ' )
    {
      filter.Text += e.KeyChar;
      promenjen = true;
    }
    else if ( e.KeyChar == '\b' && filter.Text.Length > 0 )
    {
      filter.Text = filter.Text.Substring( 0, filter.Text.Length - 1 );
      promenjen = true;
    }

    if ( promenjen )
    {
      if ( filterStari == null ) filterStari = stari;

      filterTimer.Stop();
      filterTimer.Start();
      filter.ForeColor = Color.Red;
    }
  }



  void keyDown( Object sender, KeyEventArgs e )
  {
    if ( dgv == null || table == null || view == null ) return;
    if ( e.Handled ) return;


    // CTRL+
    if ( e.KeyCode == Keys.Add &&
         e.Control &&
         !e.Alt &&
         !e.Shift )
    {
      Single fontSize = dgv.Font.SizeInPoints;

      if ( fontSize < 72 )
      {
        dgvSetFontSize( dgv, dgv.Font.FontFamily.Name, fontSize + 1 );
      }

      e.Handled = true;
      e.SuppressKeyPress = true;
    }
    else


    // CTRL-
    if ( e.KeyCode == Keys.Subtract &&
         e.Control &&
         !e.Alt &&
         !e.Shift )
    {
      Single fontSize = dgv.Font.SizeInPoints;

      if ( fontSize > 4 )
      {
        dgvSetFontSize( dgv, dgv.Font.FontFamily.Name, fontSize - 1 );
      }

      e.Handled = true;
      e.SuppressKeyPress = true;
    }
    else


    // F2
    if ( e.KeyCode == Keys.F2 &&
         !e.Control &&
         !e.Alt &&
         !e.Shift )
    {
      RotirajSort();

      e.Handled = true;
      e.SuppressKeyPress = true;
    }
    else


    // HOME, CTRL+HOME ili CTRL+PGUP
    if ( ( e.KeyCode == Keys.Home ||
         ( e.KeyCode == Keys.PageUp && e.Control ) ) &&
         !e.Alt &&
         !e.Shift )
    {
      SelektujPrvi();
      e.Handled = true;
      e.SuppressKeyPress = true;
    }
    else


    // END, CTRL+END ili CTRL+PGDN
    if ( ( e.KeyCode == Keys.End ||
         ( e.KeyCode == Keys.PageDown && e.Control ) ) &&
         !e.Alt &&
         !e.Shift )
    {
      SelektujZadnji();
      e.Handled = true;
      e.SuppressKeyPress = true;
    }
    else


    // sve ostalo
    if ( TasterAkcija != null )
    {
      bool ignorisi = false;

      switch ( e.KeyCode )
      {
        case Keys.ControlKey:
        case Keys.Menu:
        case Keys.ShiftKey:
        case Keys.Up:
        case Keys.Down:
        case Keys.PageUp:
        case Keys.PageDown:
        case Keys.Home:
        case Keys.End:

          ignorisi = true;
          break;
      }

      if ( !ignorisi )
      {
        TasterAkcija( e );
      }
    }


    // univerzalne ispravke ponasanja DataGridView-a
    if ( e.KeyCode == Keys.Back && e.Control && !e.Alt && !e.Shift )
    {
      e.SuppressKeyPress = true;
    }
    else if ( e.KeyCode == Keys.Return )
    {
      e.Handled = true;
      e.SuppressKeyPress = true;
    }
  }



  void cellMouseEvent( Object sender, DataGridViewCellMouseEventArgs e )
  {
    if ( MouseAkcija != null )
    {
      int viewRow = e.RowIndex;
      int tableRow = viewRowToTableRow( viewRow );

      if ( tableRow >= 0 )
      {
        if ( viewRow != dgv.CurrentCellAddress.Y )
        {
          dgv[ 0, viewRow ].Selected = true;
          dgv.CurrentCell = dgv[ 0, viewRow ];
          dgv.Update();
        }

        MouseAkcija( e );
      }
    }
  }



  void tick( Object sender, EventArgs e )
  {
    if ( view == null || filterTimer == null ) return;

    filterTimer.Stop();
    uspostaviFilter();
  }



  void dgvSetFontSize( DataGridView dgv, String family, Single size )
  {
    IzmenePocetak();

    resetRowCount();

    // zapamtimo stare fontove
    Font stariFontDefault = dgv.Font;
    Font stariFontPodaci = dgv.DefaultCellStyle.Font;
    Font stariFontZaglavja = dgv.ColumnHeadersDefaultCellStyle.Font;

    DataGridViewCellStyle style;

    // napravimo novi default font
    Font noviFont = new Font(family, size);
    dgv.Font = noviFont;

    // postavimo taj isti default font i za podatke
    style = dgv.DefaultCellStyle.Clone();
    style.Font = dgv.Font;
    dgv.DefaultCellStyle = style;

    // postavljamo visinu redova tabele
    dgv.RowTemplate.Height = style.Font.Height * 180 / 100;

    // prosirujemo svaku kolonu za odnos promene velicine fonta

    Double scaleFactor =

      (Double) TextRenderer.MeasureText( new String( 'M', 100 ), noviFont ).Width /
               TextRenderer.MeasureText( new String( 'M', 100 ), stariFontPodaci ).Width;

    for ( int i=0; i <= dgv.ColumnCount - 1; i++ )
    {
      dgv.Columns[ i ].Width = (int) Math.Round( dgv.Columns[ i ].Width *
                                                 scaleFactor );
    }

    // napravino i postavimo font za zaglavlja kolona
    style = dgv.ColumnHeadersDefaultCellStyle.Clone();
    style.Font = new Font( family, size, FontStyle.Bold ); // isti font ali bold
    dgv.ColumnHeadersDefaultCellStyle = style;

    dgv.ColumnHeadersHeight = style.Font.Height * 180 / 100;

    // ako su dva ista, ignorisemo jedan od njih [ samo jednom uklanjamo ]
    if ( ReferenceEquals(stariFontPodaci, stariFontDefault) )  stariFontPodaci = null;
    if ( ReferenceEquals(stariFontZaglavja, stariFontDefault) ) stariFontZaglavja = null;

    // uklonimo stare fontove ako nisu sistemski
    if (stariFontZaglavja != null)
      if (!stariFontZaglavja.IsSystemFont) stariFontZaglavja.Dispose();
    if (stariFontPodaci != null)
      if (!stariFontPodaci.IsSystemFont)   stariFontPodaci.Dispose();
    if (stariFontDefault != null)
      if (!stariFontDefault.IsSystemFont)  stariFontDefault.Dispose();

    IzmeneZavrsene();
  }

}

