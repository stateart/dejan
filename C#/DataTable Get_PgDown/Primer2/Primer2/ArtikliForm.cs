﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Primer2
{
  public partial class ArtikliForm : Form
  {
    DBArtikli db;
    DataTable tabela;
    Browse browse;


    public ArtikliForm( DBArtikli db )
    {
      InitializeComponent();

      this.db = db;
    }



    void formLoad( object sender, EventArgs e )
    {
      tabela = db.DostaviArtikle();

      if ( tabela == null )
      {
        MessageBox.Show( "Nastupio je problem pri preuzimanju podataka.\r\n\r\n" +
                         "Opis problema:\r\n" +
                         db.ErrorMessage +
                         ( db.ErrorCode > 0 ? "\r\n\r\n" + "Kod problema: " + db.ErrorCode : "" ),
                         "Problem" );
        zatvoriFormu();
        return;
      }

      tabela.PrimaryKey = new DataColumn[] { tabela.Columns[ "id" ] };  // prijavimo koja kolona
      /*                                                                  // predstavlja primarni kljuc
      DataRow row = tabela.Rows.Find( 23 );
      row[ "naziv" ]
        recno = tabela.Rows.IndexOf( row )
      */

      browse = new Browse( dgv, labelFilter, tabela );

      //DataRow row = tabela.Rows[ browse.SelektovaniRed ];
      /*
      Object[] pack = browse.SelektovaniPrimarniKljuc;
      browse.SelektovaniPrimarniKljuc = pack;

      pack[ 0 ] = god
        pack[ 1 ] = dok
      */

      //(int) pack[ 0 ]
      /*
      int id = browse.SelektovaniId;
      browse.SelektovaniId = id;
      browse.SelektujId( id );
      */

      browse.SirinaUJedinicamaM = true;  // width je izrazen u jedinicama sirine slova 'M'

      browse.DodajKolonu( "sifra",
                          "Šifra",
                          width: 7.0 );

      browse.DodajKolonu( "naziv",
                          "Naziv",
                          width: 28.0 );

      browse.DodajKolonu( "minimalnaKolicina",
                          "Min.kol.",
                          format: "F3",
                          sortAsc: "",    // blokiran za sortiranje
                          width: 10.0,
                          cellStyleFunkcija: styleZaPoljeMinKolicina );  // ovo farba u crveno kad je nula

      browse.DodajKolonu( "poslednjiUlaz",
                          "Posl.ulaz",
                          format: @"dd\.MM\.yyyy\. HH\:mm",
                          width: 11.5 );

      browse.DodajKolonu( "aktivan",
                          "Akt",
                          formatFunkcija: formatZaPoljeAktivan,  // ovo daje DA ili NE umesto, True i False
                          align: DataGridViewContentAlignment.MiddleCenter,
                          width: 6.0 );

      browse.DodajFilter( "naziv" );
      browse.DodajFilter( "sifra" );

      browse.SetupZavrsen();

      //Console.WriteLine("==================");
      //Console.WriteLine(" tabela max = " + tabela.Rows.Count);
      //DataRow row = tabela.Rows.Find(2);
      //Console.WriteLine(" find 2 = " + row["id"] + " naziv = " + row["naziv"]);
      //Console.WriteLine("recno = " + tabela.Rows.IndexOf(row));
      //Console.WriteLine("BROWSE selektovani id = " + browse.SelektovaniId);
      //Object[] pack = browse.SelektovaniPrimarniKljuc;
      //Console.WriteLine("browse.selet prim kljuc pack[0]=" + pack[0]);
      //browse.SelektovaniPrimarniKljuc = pack;
      //browse.SelektujId(4);
      //Console.WriteLine("BROWSE sektuj id ..4");
      //// Console.WriteLine("

      browse.UspostaviSort( "naziv" );
      browse.SelektujPrvi();

      browse.TasterAkcija = tasterAkcija;
      browse.MouseAkcija = mouseAkcija;

      browse.RowPreStyleFunkcija = styleZaCeoSlog;  // ovo farba bledo kad je polje "aktivan" False

      //dgv.Columns[ 1 ].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      //dgv.Columns[ 1 ].MinimumWidth = 150;
    }


    // daje DA ili NE umesto, True i False za polje "aktivan"

    String formatZaPoljeAktivan( Object value )
    {
      if ( value is bool )
      {
        if ( (bool) value ) return "DA";
                       else return "NE";
      }

      return "";
    }


    // farba u crveno kad je minimalna kolicina nula

    void styleZaPoljeMinKolicina( DataGridViewCellStyle style, Object value )
    {
      if ( value is Decimal )
      {
        Decimal d = (Decimal) value;

        if ( d == 0M )
        {
          style.ForeColor = Color.Red;
          style.SelectionForeColor = Color.Red;
        }
      }
    }


    // farba bledo kad je polje "aktivan" False

    void styleZaCeoSlog( DataGridViewCellStyle style, DataRow row )
    {
      Object o = row[ "aktivan" ];

      if ( o is bool )
      {
        if ( !(bool) o )
        {
          style.ForeColor = Color.Gray;
          style.SelectionForeColor = Color.Gray;
        }
      }
    }



    void formClosed( object sender, FormClosedEventArgs e )
    {
      browse.Ukloni();
      browse = null;

      tabela.Clear();
      tabela.Dispose();
      tabela = null;
    }



    void tasterAkcija( KeyEventArgs e )
    {
      if ( !e.Shift && !e.Control && !e.Alt )
      {
        if ( e.KeyCode == Keys.Insert )
        {
          dodajArtikal();

          e.Handled = true;
          e.SuppressKeyPress = true;
        }
        else if ( e.KeyCode == Keys.Enter )
        {
          izmeniArtikal( browse.SelektovaniId );

          e.Handled = true;
          e.SuppressKeyPress = true;
        }
        else if ( e.KeyCode == Keys.Delete )
        {
          obrisiArtikal( browse.SelektovaniId );

          e.Handled = true;
          e.SuppressKeyPress = true;
        }
        else if ( e.KeyCode == Keys.F5 )
        {
          osveziArtikle();

          e.Handled = true;
          e.SuppressKeyPress = true;
        }
        else if ( e.KeyCode == Keys.Escape )
        {
          zatvoriFormu();

          e.Handled = true;
          e.SuppressKeyPress = true;
        }
      }
    }



    void mouseAkcija( DataGridViewCellMouseEventArgs e )
    {
      if ( e.Button == MouseButtons.Left && e.Clicks == 2 )
      {
        izmeniArtikal( browse.SelektovaniId );
      }
    }



    void clickMeniDodaj( object sender, EventArgs e )
    {
      dodajArtikal();
    }



    void clickMeniIzmeni( object sender, EventArgs e )
    {
      izmeniArtikal( browse.SelektovaniId );
    }



    void clickMeniObrisi( object sender, EventArgs e )
    {
      obrisiArtikal( browse.SelektovaniId );
    }



    void clickMeniSort( object sender, EventArgs e )
    {
      browse.RotirajSort();
    }



    void clickMeniOsvezi( object sender, EventArgs e )
    {
      osveziArtikle();
    }



    void clickMeniNazad( object sender, EventArgs e )
    {
      zatvoriFormu();
    }



    void dodajArtikal()
    {
      browse.ObustaviIzmenuFiltera();

      DataTable slog = tabela.Clone();  // kopiramo samo strukturu tabele u novu praznu tabelu
                                        // ocekujemo da IzmeniArtikalForm doda novi slog u ovu
                                        // tabelu

      IzmeniArtikalForm form = new IzmeniArtikalForm( db, slog );

      if ( form.ShowDialog( this ) == DialogResult.OK )
      {
        browse.IzmenePocetak();  // pripremi se za kasnije osvezavanje prikaza tabele

        tabela.Merge( slog );    // dodajemo novi artikal u glavnu tabelu
        tabela.AcceptChanges();

        browse.IzmeneZavrseneSelektujId( (int) slog.Rows[ 0 ][ "id" ] );  // osvezavanje prikaza tabele
                                                                          // sa repozicioniranjem
      }

      form.Dispose();

      slog.Clear();
      slog.Dispose();
    }



    void izmeniArtikal( int id )
    {
      if ( id <= 0 ) return;

      browse.ObustaviIzmenuFiltera();

      // uzimamo svezu kopiju podataka (samo izabranog artikla)

      DataTable slog = db.DostaviArtikal( id );

      if ( slog == null )
      {
        MessageBox.Show( "Nastupio je problem pri preuzimanju podataka.\r\n\r\n" +
                         "Opis problema:\r\n" +
                         db.ErrorMessage +
                         ( db.ErrorCode > 0 ? "\r\n\r\n" +
                         "Kod problema: " + db.ErrorCode : "" ),
                         "Problem" );
        return;
      }

      if ( slog.Rows.Count == 0 )
      {
        MessageBox.Show( "Ovaj artikal je u medjuvremenu obrisan.", "Problem" );
        return;
      }

      // ako smo stigli do ove tacke onda tabela "slog" ima samo jedan
      // slog koji sadrzi podatke artikla koji je izabran za izmenu

      IzmeniArtikalForm form = new IzmeniArtikalForm( db, slog );

      if ( form.ShowDialog( this ) == DialogResult.OK )
      {
        browse.IzmenePocetak();   // pripremi se za kasnije osvezavanje prikaza tabele

        tabela.Merge( slog );      // ubacimo izmene u glavnu tabelu
        tabela.AcceptChanges();

        browse.IzmeneZavrsene();  // osvezavanje prikaza tabele
      }

      form.Dispose();

      slog.Clear();
      slog.Dispose();
    }



    void obrisiArtikal( int id )
    {
      if ( id <= 0 ) return;

      browse.ObustaviIzmenuFiltera();

      DataRow row = tabela.Rows.Find( id );

      if ( MessageBox.Show( this,
                            "Da li želite obrisati ovaj artikal ?\r\n\r\n" +
                              (String) row[ "sifra" ] + "  " +
                              (String) row[ "naziv" ],
                            "Brisanje",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2 ) == DialogResult.Yes )
      {
        if ( db.ObrisiArtikal( id ) )
        {
          browse.IzmenePocetak();

          row.Delete();
          tabela.AcceptChanges();

          browse.IzmeneZavrsene();
        }
        else
        {
          MessageBox.Show( "Nastupio je problem pri izmeni podataka.\r\n\r\n" +
                           "Opis problema:\r\n" +
                           db.ErrorMessage +
                           ( db.ErrorCode > 0 ? "\r\n\r\n" +
                           "Kod problema: " + db.ErrorCode : "" ),
                           "Problem" );
          return;
        }
      }
    }



    void osveziArtikle()
    {
      browse.ObustaviIzmenuFiltera();

      DataTable nova = db.DostaviArtikle();

      if ( nova == null )
      {
        MessageBox.Show( "Nastupio je problem pri preuzimanju podataka.\r\n\r\n" +
                         "Opis problema:\r\n" +
                         db.ErrorMessage +
                         ( db.ErrorCode > 0 ? "\r\n\r\n" +
                         "Kod problema: " + db.ErrorCode : "" ),
                         "Problem" );
        return;
      }

      browse.ZameniTabelu( nova );

      tabela.Clear();
      tabela.Dispose();

      tabela = nova;
    }



    void zatvoriFormu()
    {
      browse.ObustaviIzmenuFiltera();

      if ( Modal ) DialogResult = DialogResult.Cancel;
              else Close();
    }



  }
}
