using System;
using System.IO;
using System.Data;
using System.Data.Odbc;
using System.Globalization;
public class DBArtikli : DBBase  // BAG! ako nije public prijavljuje Accessibility Levels
{
  const String ImeBaze = "Primer160210";

  public DBArtikli()
  {

    //// Na svakm racunaru postaviti posebno konfiguraciju
    //// String text = File.ReadAllText( @"C:\REGISTAR\ODBC.TXT" );
      string text = "";
      text = "DRIVER={MySQL ODBC 5.2a Driver};" +  /* bez razmaka */
             "SERVER=127.0.0.1;" +
             "PORT=3306;";

      Initialize( //"DRIVER={MySQL ODBC 5.1 Driver};" +
          //"SERVER=192.168.1.251;" +
          //"PORT=3306;" +
                  text +
                  "UID=rootall;" +
                  "PWD=dir555;" +
                  "MULTI_STATEMENTS=1;" + // omoguceno vise redova
                  "NO_CACHE=1;" +
                  "NO_PROMPT=1;" +
                  "FOUND_ROWS=1");        // broji i ne izmenjene redove
  }



  public bool NapraviBazuAkoJeNema()
  {
    if ( !Connect() ) return false;

    for (;;)
    {
      try
      {
        String sql;

        sql = "CREATE DATABASE IF NOT EXISTS " + ImeBaze + " " +
              "DEFAULT CHARACTER SET utf8";
        Execute( sql );

        sql = "USE " + ImeBaze;
        Execute( sql );

        sql = "CREATE TABLE IF NOT EXISTS artikli (" +
              "id INT AUTO_INCREMENT," + // _crta
              "sifra CHAR(6) NOT NULL," +
              "naziv VARCHAR(45) NOT NULL," +
              "minimalnaKolicina DECIMAL(15,3) DEFAULT 0," +
              "poslednjiUlaz DATETIME," +
              "aktivan BIT(1) DEFAULT 1," +
              "PRIMARY KEY (id) )" +
              "DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci";
        Execute( sql );

        return true;
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return false;
      }
    }
  }



  public DataTable DostaviArtikle()
  {
    if ( !Connect() ) return null;

    for (;;)
    {
      try
      {
        Execute( "USE " + ImeBaze );
        return ExecuteQuery( "SELECT * FROM artikli" );

        // DataTable table = ExecuteQuery( "SELECT * FROM artikli" );
        // table.Locale = SerbianLatinCultureInfo;

        // return table;
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return null;
      }
    }
  }



  public DataTable DostaviArtikal( int id )
  {
    if ( !Connect() ) return null;

    for (;;)
    {
      try
      {
        Execute( "USE " + ImeBaze );
        return ExecuteQuery( "SELECT * FROM artikli WHERE id=" + id.ToString() );
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return null;
      }
    }
  }



  public bool SifraArtiklaJeSlobodna( String sifra )
  {
    if ( !Connect() ) return false;

    for (;;)
    {
      try
      {
        Execute( "USE " + ImeBaze );

        String sql;

        sql = "SELECT id FROM artikli WHERE sifra=" + SqlString( sifra );

        if ( ExecuteScalar( sql ) != null ) return false;

        return true;
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return false;
      }
    }
  }



  public bool DodajNoviArtikal( DataTable slog )
  {
    if ( !Connect() ) return false;

    for (;;)
    {
      try
      {
        Execute( "USE " + ImeBaze );

        TransactionStart();

        String sql;

        sql = "SELECT id FROM artikli WHERE sifra=" +
              SqlString( slog.Rows[ 0 ][ "sifra" ] ) + " " +
              "FOR UPDATE";

        if ( ExecuteScalar( sql ) != null )
        {
          TransactionRollback();

          ErrorMessage = "�ifra je ve� zauzeta.";
          ErrorCode = -101;
          return false;
        }

        sql = "INSERT INTO artikli (" +
              "sifra,naziv,minimalnaKolicina,poslednjiUlaz,aktivan" +
              ") VALUES (" +
              SqlString( slog.Rows[ 0 ][ "sifra" ] ) + "," +
              SqlString( slog.Rows[ 0 ][ "naziv" ] ) + "," +
              SqlNumber( slog.Rows[ 0 ][ "minimalnaKolicina" ], 3 ) + "," +
              SqlDateTime( slog.Rows[ 0 ][ "poslednjiUlaz" ] ) + "," +
              SqlBool( slog.Rows[ 0 ][ "aktivan" ] ) + ")";

        Execute( sql );

        sql = "SELECT last_insert_id()";

        int id = CastToInt( ExecuteScalar( sql ) );

        TransactionCommit();

        slog.Rows[ 0 ][ "id" ] = id;

        return true;
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return false;
      }
    }
  }



  public bool ObrisiArtikal( int id )
  {
    if ( !Connect() ) return false;

    for (;;)
    {
      try
      {
        Execute( "USE " + ImeBaze );

        String sql;

        sql = "DELETE FROM artikli WHERE id=" + id.ToString();

        if ( Execute( sql ) != 1 )
        {
          ErrorCode = -1;
          ErrorMessage = "DELETE nije vratio 1";
          return false;
        }

        return true;
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return false;
      }
    }
  }



  public bool IzmeniArtikal( DataTable izmenjen,
                             DataTable original )
  {
    if ( !Connect() ) return false;

    for (;;)
    {
      try
      {
        Execute( "USE " + ImeBaze );

        TransactionStart();

        int id = (int) original.Rows[ 0 ][ "id" ];

        String sql;
        DataTable tekuci = null;

        try
        {
          sql = "SELECT * FROM artikli " +
                "WHERE id=" + id.ToString() + " " +
                "FOR UPDATE";

          tekuci = ExecuteQuery( sql );

          if ( tekuci.Rows.Count == 0 )
          {
            TransactionRollback();

            ErrorMessage = "Artikal je u me�uvremenu obrisan.";
            ErrorCode = -1;
            return false;
          }

          bool prvi = true;

          sql = "UPDATE artikli SET ";


          // sifra

          if ( !poljeJeIsto( izmenjen, original, "sifra" ) )
          {
            if ( !poljeJeIsto( izmenjen, tekuci, "sifra" ) )
            {
              // proveravamo da li je sifra slobodna

              String subsql;

              subsql = "SELECT id FROM artikli WHERE sifra=" +
                       SqlString( (String) izmenjen.Rows[ 0 ][ "sifra" ] ) + " " +
                       "FOR UPDATE";

              if ( ExecuteScalar( subsql ) != null )
              {
                TransactionRollback();

                ErrorMessage = "�ifra je ve� zauzeta.";
                ErrorCode = -101;
                return false;
              }


              // nastavljamo sa proverom konflikta

              if ( !poljeJeIsto( tekuci, original, "sifra" ) )
              {
                TransactionRollback();

                ErrorMessage = "�ifra artikla je u me�uvremenu promenjena.";
                ErrorCode = -1;
                return false;
              }

              if ( prvi ) prvi = false;
                     else sql += ", ";

              sql += "sifra=" + SqlString( izmenjen.Rows[ 0 ][ "sifra" ] );
            }
          }


          // naziv

          if ( !poljeJeIsto( izmenjen, original, "naziv" ) )
          {
            if ( !poljeJeIsto( izmenjen, tekuci, "naziv" ) )
            {
              if ( !poljeJeIsto( tekuci, original, "naziv" ) )
              {
                TransactionRollback();

                ErrorMessage = "Naziv artikla je u me�uvremenu promenjen.";
                ErrorCode = -1;
                return false;
              }

              if ( prvi ) prvi = false;
              else sql += ", ";

              sql += "naziv=" + SqlString( izmenjen.Rows[ 0 ][ "naziv" ] );
            }
          }


          // minimalnaKolicina

          if ( !poljeJeIsto( izmenjen, original, "minimalnaKolicina" ) )
          {
            if ( !poljeJeIsto( izmenjen, tekuci, "minimalnaKolicina" ) )
            {
              if ( !poljeJeIsto( tekuci, original, "minimalnaKolicina" ) )
              {
                TransactionRollback();

                ErrorMessage = "Minimalna koli�ina artikla je u me�uvremenu promenjena.";
                ErrorCode = -1;
                return false;
              }

              if ( prvi ) prvi = false;
              else sql += ", ";

              sql += "minimalnaKolicina=" +
                        SqlNumber( izmenjen.Rows[ 0 ][ "minimalnaKolicina" ], 3 );
            }
          }


          // poslednjiUlaz

          if ( !poljeJeIsto( izmenjen, original, "poslednjiUlaz" ) )
          {
            if ( !poljeJeIsto( izmenjen, tekuci, "poslednjiUlaz" ) )
            {
              if ( !poljeJeIsto( tekuci, original, "poslednjiUlaz" ) )
              {
                TransactionRollback();

                ErrorMessage = "Poslednji ulaz artikla je u me�uvremenu promenjen.";
                ErrorCode = -1;
                return false;
              }

              if ( prvi ) prvi = false;
              else sql += ", ";

              sql += "poslednjiUlaz=" +
                        SqlDateTime( izmenjen.Rows[ 0 ][ "poslednjiUlaz" ] );
            }
          }


          // aktivan

          if ( !poljeJeIsto( izmenjen, original, "aktivan" ) )
          {
            if ( !poljeJeIsto( izmenjen, tekuci, "aktivan" ) )
            {
              if ( prvi ) prvi = false;
              else sql += ", ";

              sql += "aktivan=" +
                        SqlBool( izmenjen.Rows[ 0 ][ "aktivan" ] );
            }
          }


          // ako ima sta, izvrsi

          if ( !prvi )
          {
            sql += " WHERE id=" + id.ToString();

            Execute( sql );

            tekuci.Clear();
            tekuci.Dispose();
            tekuci = null;

            sql = "SELECT * FROM artikli " +
                  "WHERE id=" + id.ToString() + " " +
                  "FOR UPDATE";

            tekuci = ExecuteQuery( sql );
          }


          // pripremi tekuce stanje da bude vraceno
          // u glavnu tabelu

          original.Rows.Clear();
          original.Merge( tekuci );

        }
        finally
        {
          if ( tekuci != null )
          {
            tekuci.Clear();
            tekuci.Dispose();
            tekuci = null;
          }
        }

        TransactionCommit();

        return true;
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return false;
      }
    }
  }



  bool poljeJeIsto( DataTable dt1,
                    DataTable dt2,
                    String polje )
               
  {
    return ( dt1.Rows[ 0 ][ polje ].Equals( dt2.Rows[ 0 ][ polje ] ) );
  }



/* SABLON

  public ... Sablon( ... )
  {
    if ( !Connect() ) return ...;  // negativan rezultat

    for (;;)
    {
      try
      {
        TransactionStart();
        ...
        TransactionCommit();

        return ...;                // pozitivan rezultat
      }
      catch ( Exception ex )
      {
        if ( !MySqlProcessException( ex ) ) return ...;  // negativan rezultat
      }
    }
  }
*/

}

