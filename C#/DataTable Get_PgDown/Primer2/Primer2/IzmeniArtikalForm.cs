﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StateArtForms;

namespace Primer2
{
  public partial class IzmeniArtikalForm : StateArtForm  // ako se koristi VS2012 treba :Form umesto :FormaBase
  {
    DBArtikli db;
    DataTable slog;
    bool novi;


    public IzmeniArtikalForm( DBArtikli db, DataTable slog )
    {
      InitializeComponent();

      this.db = db;
      this.slog = slog;

      novi = ( slog.Rows.Count == 0 );
    }



    void formLoad( object sender, EventArgs e )
    {
      if ( novi )
      {
        Text = "Dodavanje artikla";

        tbSifra.Text = "";
        tbNaziv.Text = "";
        nbMinKolicina.Value = 0.0;
        dtbPoslUlaz.Value = DateTime.Now;
        cbAktivan.Checked = true;

        btPotvrdi.Text = "&Dodaj";
      }
      else
      {
        Text = "Izmena artikla";

        tbSifra.Text = razresi( slog.Rows[ 0 ][ "sifra" ], "" );
        tbNaziv.Text = razresi( slog.Rows[ 0 ][ "naziv" ], "" );

        nbMinKolicina.Value =
          razresiDouble( slog.Rows[ 0 ][ "minimalnaKolicina" ], 0.0 );

        dtbPoslUlaz.Value = razresi( slog.Rows[ 0 ][ "poslednjiUlaz" ], DateTime.MinValue );
        cbAktivan.Checked = razresi( slog.Rows[ 0 ][ "aktivan" ], true );

        btPotvrdi.Text = "&Izmeni";
      }

      PostaviNavigaciju();
    }



    T razresi<T>( Object o, T null_value )
    {
      if ( o is T ) return (T) o;
      return null_value;
    }



    Double razresiDouble( Object o, Double null_value )
    {
      if ( o is Double ) return (Double) o;
      if ( o is Decimal ) return (Double) (Decimal) o;
      if ( o is int ) return (Double) (int) o;
      if ( o is long ) return (Double) (long) o;
      return null_value;
    }



    void leaveSifra( object sender, EventArgs e )
    {
      String sifra = tbSifra.Text.Trim();

      if ( sifra.Length > 0 )
      {
        tbSifra.Text = sifra;

        if ( novi )
        {
          if ( !db.SifraArtiklaJeSlobodna( sifra ) )
          {
            labelSifraProblem.Show();
            tbSifra.ForeColor = Color.Red;
            return;
          }
        }
        else
        {
          String original = razresi( slog.Rows[ 0 ][ "sifra" ], "" );

          if ( sifra != original.Trim() )
          {
            if ( !db.SifraArtiklaJeSlobodna( sifra ) )
            {
              labelSifraProblem.Show();
              tbSifra.ForeColor = Color.Red;
              return;
            }
          }
        }
      }

      labelSifraProblem.Hide();
      tbSifra.ResetForeColor();
    }



    void clickOdustani( object sender, EventArgs e )
    {
      DialogResult = DialogResult.Cancel;
    }



    void clickPotvrdi( object sender, EventArgs e )
    {
      // provera svakog polja pojedinacno

      String sifra = tbSifra.Text.Trim();

      if ( sifra.Length == 0 )
      {
        tbSifra.Focus();
        return;
      }


      String naziv = tbNaziv.Text.Trim();

      if ( naziv.Length == 0 )
      {
        tbNaziv.Focus();
        return;
      }


      Double minKolicina = nbMinKolicina.Value;

      if ( minKolicina < 0.0 || minKolicina > 999999 )
      {
        nbMinKolicina.Focus();
        return;
      }


      if ( !dtbPoslUlaz.IsValueValid )
      {
        dtbPoslUlaz.Focus();
        return;
      }

      Object poslUlaz;

      if ( dtbPoslUlaz.Value == DateTime.MinValue )
      {
        poslUlaz = DBNull.Value;  // NULL u SQL-u
      }
      else
      {
        poslUlaz = dtbPoslUlaz.Value;
      }


      bool aktivan = cbAktivan.Checked;


      // sva polja su sada proverena, ovde pocinje upis

      DataTable podaci = slog.Clone();  // pripremimo novu praznu tabelu sa
                                        // strukturom osnovne tabele

      DataRow noviSlog = podaci.NewRow();  // kreiramo novi slog

      noviSlog[ "id" ] = -1;               // popunimo slog sa proverenim
      noviSlog[ "sifra" ] = sifra;         // podacima iz forme
      noviSlog[ "naziv" ] = naziv;
      noviSlog[ "minimalnaKolicina" ] = minKolicina;
      noviSlog[ "poslednjiUlaz" ] = poslUlaz;
      noviSlog[ "aktivan" ] = aktivan;

      podaci.Rows.Add( noviSlog );  // dodamo pripremljeni slog u novu tabelu


      if ( novi )  // *** UPIS NOVOG ARTIKLA ***
      {
        // pripremljenu tabelu predajemo izvoru podataka da ih upise u bazu

        if ( db.DodajNoviArtikal( podaci ) )
        {
          // upisivanje je uspelo, db.DodajNoviArtikal je popunio polje "id"

          slog.Merge( podaci );  // ubacimo podatke iz privremene tabele u
                                 // tabelu slog koja je predata u konstruktoru

          DialogResult = DialogResult.OK;  // oznacimo formu za zatvaranje
                                           // cim se ova metoda zavrsi
        }
        else
        {
          // upisivanje nije uspelo

          if ( db.ErrorCode == -101 )  // sifra je vec zauzeta
          {
            tbSifra.ForeColor = Color.Red;
            labelSifraProblem.Show();

            tbSifra.Focus();
          }
          else                         // neki drugi problem
          {
            MessageBox.Show( "Nastupio je problem pri upisu podataka.\r\n\r\n" +
                             "Opis problema:\r\n" +
                             db.ErrorMessage +
                             ( db.ErrorCode > 0 ? "\r\n\r\n" +
                             "Kod problema: " + db.ErrorCode : "" ),
                             "Problem" );
          }
        }
      }
      else // *** IZMENA ARTIKLA ***
      {
        podaci.Rows[ 0 ][ "id" ] = slog.Rows[ 0 ][ "id" ];  // u slucaju izmene
                                                            // imamo "id" sloga
        if ( db.IzmeniArtikal( podaci, slog ) )
        {
          // izmena je uspela, db.IzmeniArtikal je finalno stanje vratio
          // u tabeli "slog", tako da ovde vise nema sta da se radi

          DialogResult = DialogResult.OK;  // oznacimo formu za zatvaranje
                                           // cim se ova metoda zavrsi
        }
        else
        {
          // izmena nije uspela

          if ( db.ErrorCode == -101 )  // sifra je vec zauzeta
          {
            tbSifra.ForeColor = Color.Red;
            labelSifraProblem.Show();

            tbSifra.Focus();
          }
          else                         // neki drugi problem
          {
            MessageBox.Show( "Nastupio je problem pri upisu podataka.\r\n\r\n" +
                              "Opis problema:\r\n" +
                              db.ErrorMessage +
                              ( db.ErrorCode > 0 ? "\r\n\r\n" +
                              "Kod problema: " + db.ErrorCode : "" ),
                              "Problem" );
          }
        }
      }

      podaci.Clear();    // privremena tabela vise nije potrebna
      podaci.Dispose();
    }

  }
}
