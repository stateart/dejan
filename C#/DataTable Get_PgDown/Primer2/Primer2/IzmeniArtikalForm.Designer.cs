﻿namespace Primer2
{
  partial class IzmeniArtikalForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
      if ( disposing && ( components != null ) )
      {
        components.Dispose();
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.label1 = new System.Windows.Forms.Label();
        this.tbSifra = new System.Windows.Forms.TextBox();
        this.label2 = new System.Windows.Forms.Label();
        this.tbNaziv = new System.Windows.Forms.TextBox();
        this.label3 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.cbAktivan = new System.Windows.Forms.CheckBox();
        this.btPotvrdi = new System.Windows.Forms.Button();
        this.btOdustani = new System.Windows.Forms.Button();
        this.labelSifraProblem = new System.Windows.Forms.Label();
        this.dtbPoslUlaz = new StateArtForms.DateTimeBox();
        this.nbMinKolicina = new StateArtForms.NumBox();
        this.SuspendLayout();
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(18, 18);
        this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(31, 13);
        this.label1.TabIndex = 0;
        this.label1.Text = "Šifra:";
        // 
        // tbSifra
        // 
        this.tbSifra.Location = new System.Drawing.Point(74, 15);
        this.tbSifra.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.tbSifra.MaxLength = 6;
        this.tbSifra.Name = "tbSifra";
        this.tbSifra.Size = new System.Drawing.Size(49, 20);
        this.tbSifra.TabIndex = 1;
        this.tbSifra.Leave += new System.EventHandler(this.leaveSifra);
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(18, 41);
        this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(37, 13);
        this.label2.TabIndex = 2;
        this.label2.Text = "Naziv:";
        // 
        // tbNaziv
        // 
        this.tbNaziv.Location = new System.Drawing.Point(74, 38);
        this.tbNaziv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.tbNaziv.MaxLength = 45;
        this.tbNaziv.Name = "tbNaziv";
        this.tbNaziv.Size = new System.Drawing.Size(136, 20);
        this.tbNaziv.TabIndex = 3;
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(18, 63);
        this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(44, 13);
        this.label3.TabIndex = 4;
        this.label3.Text = "Min.kol:";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(18, 89);
        this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(52, 13);
        this.label4.TabIndex = 6;
        this.label4.Text = "Posl.ulaz:";
        // 
        // cbAktivan
        // 
        this.cbAktivan.AutoSize = true;
        this.cbAktivan.Location = new System.Drawing.Point(74, 111);
        this.cbAktivan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.cbAktivan.Name = "cbAktivan";
        this.cbAktivan.Size = new System.Drawing.Size(62, 17);
        this.cbAktivan.TabIndex = 8;
        this.cbAktivan.Text = "Aktivan";
        // 
        // btPotvrdi
        // 
        this.btPotvrdi.Location = new System.Drawing.Point(74, 143);
        this.btPotvrdi.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.btPotvrdi.Name = "btPotvrdi";
        this.btPotvrdi.Size = new System.Drawing.Size(64, 22);
        this.btPotvrdi.TabIndex = 9;
        this.btPotvrdi.Text = "&Potvrdi";
        this.btPotvrdi.Click += new System.EventHandler(this.clickPotvrdi);
        // 
        // btOdustani
        // 
        this.btOdustani.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        this.btOdustani.Location = new System.Drawing.Point(145, 143);
        this.btOdustani.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.btOdustani.Name = "btOdustani";
        this.btOdustani.Size = new System.Drawing.Size(64, 22);
        this.btOdustani.TabIndex = 10;
        this.btOdustani.Text = "&Odustani";
        this.btOdustani.Click += new System.EventHandler(this.clickOdustani);
        // 
        // labelSifraProblem
        // 
        this.labelSifraProblem.AutoSize = true;
        this.labelSifraProblem.ForeColor = System.Drawing.Color.Red;
        this.labelSifraProblem.Location = new System.Drawing.Point(127, 17);
        this.labelSifraProblem.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
        this.labelSifraProblem.Name = "labelSifraProblem";
        this.labelSifraProblem.Size = new System.Drawing.Size(82, 13);
        this.labelSifraProblem.TabIndex = 11;
        this.labelSifraProblem.Text = "Šifra je zauzeta.";
        this.labelSifraProblem.Visible = false;
        // 
        // dtbPoslUlaz
        // 
        this.dtbPoslUlaz.InputFormat = StateArtForms.DateTimeBox.Format.DateOptionalTime;
        this.dtbPoslUlaz.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
        this.dtbPoslUlaz.Location = new System.Drawing.Point(74, 84);
        this.dtbPoslUlaz.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.dtbPoslUlaz.Mask = "00\\.00\\.0000\\. 00\\:00\\:00";
        this.dtbPoslUlaz.Name = "dtbPoslUlaz";
        this.dtbPoslUlaz.Size = new System.Drawing.Size(102, 20);
        this.dtbPoslUlaz.TabIndex = 7;
        this.dtbPoslUlaz.Value = new System.DateTime(((long)(0)));
        // 
        // nbMinKolicina
        // 
        this.nbMinKolicina.AllowNegative = false;
        this.nbMinKolicina.Decimals = 3;
        this.nbMinKolicina.Location = new System.Drawing.Point(74, 61);
        this.nbMinKolicina.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.nbMinKolicina.MaxLength = 20;
        this.nbMinKolicina.Name = "nbMinKolicina";
        this.nbMinKolicina.Size = new System.Drawing.Size(76, 20);
        this.nbMinKolicina.TabIndex = 5;
        this.nbMinKolicina.Text = "0.000";
        this.nbMinKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // IzmeniArtikalForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.CancelButton = this.btOdustani;
        this.ClientSize = new System.Drawing.Size(230, 184);
        this.Controls.Add(this.nbMinKolicina);
        this.Controls.Add(this.dtbPoslUlaz);
        this.Controls.Add(this.labelSifraProblem);
        this.Controls.Add(this.btOdustani);
        this.Controls.Add(this.btPotvrdi);
        this.Controls.Add(this.cbAktivan);
        this.Controls.Add(this.tbNaziv);
        this.Controls.Add(this.tbSifra);
        this.Controls.Add(this.label4);
        this.Controls.Add(this.label3);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.label1);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
        this.MaximizeBox = false;
        this.Name = "IzmeniArtikalForm";
        this.PotvrdiFormuButton = this.btPotvrdi;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "IzmeniArtikalForm";
        this.Load += new System.EventHandler(this.formLoad);
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbSifra;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox tbNaziv;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.CheckBox cbAktivan;
    private System.Windows.Forms.Button btPotvrdi;
    private System.Windows.Forms.Button btOdustani;
    private System.Windows.Forms.Label labelSifraProblem;
    private StateArtForms.DateTimeBox dtbPoslUlaz;
    private StateArtForms.NumBox nbMinKolicina;

  }
}