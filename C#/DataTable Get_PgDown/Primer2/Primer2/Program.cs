﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Primer2
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault( false );

      DBArtikli db = new DBArtikli(); // ako se objekat prenese u samu formu, svaka forma ima odvojenu konekciju

      if ( !db.NapraviBazuAkoJeNema() )
      {
        MessageBox.Show( "Nastupio je problem u radu sa izvorom podataka.\r\n\r\n" +
                         "Opis problema:\r\n" +
                         db.ErrorMessage +
                         ( db.ErrorCode > 0 ? "\r\n\r\n" + "Kod problema: " + db.ErrorCode : "" ),
                         "Problem" );
        return;
      }

      Application.Run( new ArtikliForm( db ) );
    }
  }
}
