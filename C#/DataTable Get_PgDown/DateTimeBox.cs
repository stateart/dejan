
using System;
using System.Globalization;
using System.Windows.Forms;
using System.ComponentModel;

namespace StateArtForms
{
  public class DateTimeBox : MaskedTextBox
  {
    public enum Format
    {
      DateTime,
      DateOnly,
      DateOptionalTime,
      DateTimeNoSeconds,
      DateTimeOptionalSeconds,
      TimeOnly,
      TimeOnlyNoSeconds,
      TimeOnlyOptionalSeconds
    }

    [Category( "DateTime" )]
    [Description( "Gets or sets input format." )]
    [DefaultValue( DateTimeBox.Format.DateTime )]

    public Format InputFormat
    {
      get
      {
        return m_format;
      }

      set
      {
        DateTime temp = Value;
        m_format = value;
        base.Mask = MaskString;
        Value = temp;
      }
    }


    [Category( "DateTime" )]
    [Description( "If true input format uses only two digits for year." )]
    [DefaultValue( false )]

    public bool TwoDigitYear
    {
      get
      {
        return m_two_digit_year;
      }

      set
      {
        DateTime temp = Value;
        m_two_digit_year = value;
        base.Mask = MaskString;
        Value = temp;
      }
    }

    [Category( "DateTime" )]
    [Description( "DateTime value." )]
    public DateTime Value
    {
      get
      {
        DateTime dt;
        GetValue( out dt );
        return dt;
      }

      set
      {
        if ( value == DateTime.MinValue )
        {
          base.Text = "";
          return;
        }

        base.Text = value.ToString( FormatString,
                                    CultureInfo.InvariantCulture );
      }
    }


    public bool IsValueValid
    {
      get
      {
        DateTime dt;

        bool valid = GetValue( out dt );

        return valid;
      }
    }


    public DateTimeBox()
    {
      m_format = Format.DateTime;
      m_two_digit_year = false;
      base.InsertKeyMode = InsertKeyMode.Overwrite;

      base.Mask = MaskString;
      base.Text = "";
    }


    bool GetValue( out DateTime value )
    {
      DateTime dt;
      bool valid = false;

      if ( base.Text.Trim() == TrimmedEmptyValue )
      {
        dt = DateTime.MinValue;
        valid = true;
      }
      else if ( DateTime.TryParseExact( base.Text.Trim(),
                                        FormatString,
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.AssumeLocal,
                                        out dt ) )
      {
        if ( dt == DateTime.MinValue ) base.Text = "";
        valid = true;
      }

      if ( !valid )
      {
        switch ( m_format )
        {
          case Format.DateOptionalTime:
          {
            String year = m_two_digit_year ? "yy" : "yyyy";

            valid = DateTime.TryParseExact( base.Text.Trim(),
                                            @"dd\.MM\." + year + @"\. HH\:mm\:",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.AssumeLocal,
                                            out dt );
            if ( !valid )
            {
              valid = DateTime.TryParseExact( base.Text.Trim(),
                                              @"dd\.MM\." + year + @"\. HH\:  \:",
                                              CultureInfo.InvariantCulture,
                                              DateTimeStyles.AssumeLocal,
                                              out dt );
              if ( !valid )
              {
                valid = DateTime.TryParseExact( base.Text.Trim(),
                                                @"dd\.MM\." + year + @"\.   \:  \:",
                                                CultureInfo.InvariantCulture,
                                                DateTimeStyles.AssumeLocal,
                                                out dt );

                if ( !valid ) dt = DateTime.MinValue;
              }
            }

            break;
          }


          case Format.DateTimeOptionalSeconds:
          {
            String year = m_two_digit_year ? "yy" : "yyyy";

            valid = DateTime.TryParseExact( base.Text.Trim(),
                                            @"dd\.MM\." + year + @"\. HH\:mm\:",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.AssumeLocal,
                                            out dt );

            if ( !valid ) dt = DateTime.MinValue;

            break;
          }


          case Format.TimeOnlyOptionalSeconds:
          {
            valid = DateTime.TryParseExact( base.Text.Trim(),
                                            @"HH\:mm\:",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.AssumeLocal,
                                            out dt );
            if ( !valid ) dt = DateTime.MinValue;

            break;
          }
        }
      }

      value = dt;

      return valid;
    }


    protected override void OnEnter( EventArgs e )
    {
      
      base.OnEnter( e );

      BeginInvoke( (Action) ( () =>
                              {
                                SelectionStart = 0;
                                SelectionLength = 0;
                                base.InsertKeyMode = InsertKeyMode.Overwrite;
                              }
                            ) );
    }


    protected override void OnKeyPress( KeyPressEventArgs e )
    {
      if ( e.KeyChar == '\r' ) e.Handled = true;

      base.OnKeyPress( e );
    }


    protected override void OnLeave( EventArgs e )
    {
      DateTime dt = Value;
      if ( dt != DateTime.MinValue ) Value = dt;

      base.OnLeave( e );
    }


    [Browsable( false )]
    public new String Text
    {
      get { return base.Text; }
      set { }
    }


    [Browsable( false )]
    public new String[] Lines
    {
      get { return base.Lines; }
      set { }
    }


    [Browsable( false )]
    public new bool Multiline
    {
      get { return base.Multiline; }
      set { }
    }


    [Browsable( false )]
    public new Char PasswordChar
    {
      get { return base.PasswordChar; }
      set { }
    }


    [Browsable( false )]
    public new bool UseSystemPasswordChar
    {
      get { return base.UseSystemPasswordChar; }
      set { }
    }


    [Browsable( false )]
    public new bool WordWrap
    {
      get { return base.WordWrap; }
      set { }
    }


    [Browsable( false )]
    public new String Mask
    {
      get { return base.Mask; }
      set { }
    }


    [Browsable( false )]
    public new InsertKeyMode InsertKeyMode
    {
      get { return InsertKeyMode.Overwrite; }
      set { }
    }


    String FormatString
    {
      get
      {
        String year = m_two_digit_year ? "yy" : "yyyy";

        switch ( m_format )
        {
          case Format.DateTime:
            return @"dd\.MM\." + year + @"\. HH\:mm\:ss";

          case Format.DateOnly:
            return @"dd\.MM\." + year + @"\.";

          case Format.DateOptionalTime:
            return @"dd\.MM\." + year + @"\. HH\:mm\:ss";

          case Format.DateTimeNoSeconds:
            return @"dd\.MM\." + year + @"\. HH\:mm";

          case Format.DateTimeOptionalSeconds:
            return @"dd\.MM\." + year + @"\. HH\:mm\:ss";

          case Format.TimeOnly:
            return @"HH\:mm\:ss";

          case Format.TimeOnlyNoSeconds:
            return @"HH\:mm";

          case Format.TimeOnlyOptionalSeconds:
            return @"HH\:mm\:ss";
        }

        throw new Exception( "DateTimeBox invalid format." );
      }
    }


    String MaskString
    {
      get
      {
        String year = m_two_digit_year ? "00" : "0000";

        switch ( m_format )
        {
          case Format.DateTime:
            return @"00\.00\." + year + @"\. 00\:00\:00";

          case Format.DateOnly:
            return @"00\.00\." + year + @"\.";

          case Format.DateOptionalTime:
            return @"00\.00\." + year + @"\. 00\:00\:00";

          case Format.DateTimeNoSeconds:
            return @"00\.00\." + year + @"\. 00\:00";

          case Format.DateTimeOptionalSeconds:
            return @"00\.00\." + year + @"\. 00\:00\:00";

          case Format.TimeOnly:
            return @"00\:00\:00";

          case Format.TimeOnlyNoSeconds:
            return @"00\:00";

          case Format.TimeOnlyOptionalSeconds:
            return @"00\:00\:00";
        }

        throw new Exception( "DateTimeBox invalid format." );
      }
    }


    String TrimmedEmptyValue
    {
      get
      {
        String year = m_two_digit_year ? "  " : "    ";

        switch ( m_format )
        {
          case Format.DateTime:
            return ".  ." + year + ".   :  :";

          case Format.DateOnly:
            return ".  ." + year + ".";

          case Format.DateOptionalTime:
            return ".  ." + year + ".   :  :";

          case Format.DateTimeNoSeconds:
            return ".  ." + year + ".   :";

          case Format.DateTimeOptionalSeconds:
            return ".  ." + year + ".   :  :";

          case Format.TimeOnly:
            return ":  :";

          case Format.TimeOnlyNoSeconds:
            return ":";

          case Format.TimeOnlyOptionalSeconds:
            return ":  :";
        }

        throw new Exception( "DateTimeBox invalid format." );
      }
    }


    Format m_format;
    bool m_two_digit_year;
  }
}
