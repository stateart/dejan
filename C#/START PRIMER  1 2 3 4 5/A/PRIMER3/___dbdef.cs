using System;

// Specialized classes of primer.dbf: MTPrimer, MTIPrimer, MTPrimerBase and MTPrimerRow

public interface MTPrimerRow : MTRow
{
  String    SIFRA        { get; set; }
  String    NAZIV        { get; set; }
  Double    VREDNOST     { get; set; }
  int       POZICIJA     { get; set; }
  DateTime  DATUM        { get; set; }
  bool      VAZECI       { get; set; }

  MTPrimerRow GetPrimerRow();
  MTPrimerRow GetBlankPrimerRow();
}

public interface MTPrimerBase : MTBase, MTPrimerRow {}

public class MTPrimer : MT, MTPrimerBase
{
  public String SIFRA
  {
    get { return GetString(1); }
    set { Put( 1, value ); }
  }

  public String NAZIV
  {
    get { return GetString(2); }
    set { Put( 2, value ); }
  }

  public Double VREDNOST
  {
    get { return GetDouble(3); }
    set { Put( 3, value ); }
  }

  public int POZICIJA
  {
    get { return GetInt(4); }
    set { Put( 4, value ); }
  }

  public DateTime DATUM
  {
    get { return GetDateTime(5); }
    set { Put( 5, value ); }
  }

  public bool VAZECI
  {
    get { return GetBool(6); }
    set { Put( 6, value ); }
  }

  public MTPrimerRow GetPrimerRow()
  {
    return GetRow<MTPrimerRowImpl>();
  }

  public MTPrimerRow GetBlankPrimerRow()
  {
    return GetBlankRow<MTPrimerRowImpl>();
  }
}

public class MTIPrimer : MTI, MTPrimerBase
{
  public String SIFRA
  {
    get { return GetString(1); }
    set { Put( 1, value ); }
  }

  public String NAZIV
  {
    get { return GetString(2); }
    set { Put( 2, value ); }
  }

  public Double VREDNOST
  {
    get { return GetDouble(3); }
    set { Put( 3, value ); }
  }

  public int POZICIJA
  {
    get { return GetInt(4); }
    set { Put( 4, value ); }
  }

  public DateTime DATUM
  {
    get { return GetDateTime(5); }
    set { Put( 5, value ); }
  }

  public bool VAZECI
  {
    get { return GetBool(6); }
    set { Put( 6, value ); }
  }

  public MTPrimerRow GetPrimerRow()
  {
    return GetRow<MTPrimerRowImpl>();
  }

  public MTPrimerRow GetBlankPrimerRow()
  {
    return GetBlankRow<MTPrimerRowImpl>();
  }
}

public class MTPrimerRowImpl : MTRowImpl, MTPrimerRow
{
  public String SIFRA
  {
    get { return GetString(1); }
    set { Put( 1, value ); }
  }

  public String NAZIV
  {
    get { return GetString(2); }
    set { Put( 2, value ); }
  }

  public Double VREDNOST
  {
    get { return GetDouble(3); }
    set { Put( 3, value ); }
  }

  public int POZICIJA
  {
    get { return GetInt(4); }
    set { Put( 4, value ); }
  }

  public DateTime DATUM
  {
    get { return GetDateTime(5); }
    set { Put( 5, value ); }
  }

  public bool VAZECI
  {
    get { return GetBool(6); }
    set { Put( 6, value ); }
  }

  public MTPrimerRow GetPrimerRow()
  {
    return GetRow<MTPrimerRowImpl>();
  }

  public MTPrimerRow GetBlankPrimerRow()
  {
    return GetBlankRow<MTPrimerRowImpl>();
  }
}

