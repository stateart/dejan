
  Primeri su izdvojeni u poddirektorijume. Svaki se kompajlira pozivanjem
  A.BAT u datom direktorijumu, ali prethodno treba uraditi sledece:

  PRE KOMPAJLIRANJA PREKOPIRATI CSC.BAT I WP.CS U DIREKTORIJUM PRIMERA.
  TAKODJE, UZ SVAKI REZULTUJUCI .EXE JE POTREBANO I DA WPNET*.DLL BUDE
  PRISUTAN U DIREKTORIJUMU IZ KOJEG SE POKRECE TAJ .EXE ILI "U PUTANJI".

  Spisak primera:

    PRIMER1 - konzolni program koji prikazuje kako se izvode osnovne
              operacije sa memorijskim tabelama u WinProxy-u za C#

    PRIMER2 - isto kao PRIMER1, ali koristi alternativni nacin #1 citanja
              i pisanja u polja

    PRIMER3 - isto kao PRIMER1, ali koristi alternativni nacin #2 citanja
              i pisanja u polja, ZAHTEVA DBDEFGEN.EXE DA BUDE PRISUTAN U
              DIREKTORIJUMU ILI "U PUTANJI" ZA VREME KOMPAJLIRANJA
              
    PRIMER4 - konzolni program demonstrira upotrebu E1 i E2 programa i
              upotrebu E2 filtra

    PRIMER5 - Windows program demonstrira upotrebu Windows Forms zajedno sa
              WinProxy memorijskim tabelama, takodje demonstrira upotrebu
              NumBox kontrole, ZAHTEVA NUMBOX.CS DA BUDE U DIREKTORIJUMU
              ZA VREME KOMPAJLIRANJA



  Sta je sta:

    WPNet.dll - mora biti prisutan u folderu odakle se pokrecu kompajlirani
                programi. (WPNet.dll je WINPROXY.EXE prepakovan u .DLL oblik,
                sve sto ubuduce bude sadrzao WINPROXY.EXE bice i u ovom .DLL-u) 

    WPNetDbg.dll - je debug verzija WPNet.dll (WINPROXY.DEBUG)

    WP.CS     - slicno kao WPFUNC.PRG, omogucava upotrebu WinProxy funkcija
                iz C#-a, kompajlira se u svaki .EXE, ili se moze realizovati
                kao poseban .DLL

    CSC.BAT   - univerzalna batch datoteka za kompajliranje C# source koda
                (pronalazi i poziva CSC.EXE (C# kompajler) na lokalnom
                racunaru, svi parametri dati sa CSC.BAT se prenose kao
                parametri za CSC.EXE, za kratak opis podrzanih parametara
                pozvati CSC /?)

    CLIP2NET.TXT - tekst dokument koji sadrzi spisak izabranih funkcija i 
                   koncepata iz Clipper-a i njihovih ekvivalenata u
                   C#/.NET-u

    DBDefGen.exe - generise source kod koji omogucava pristup poljima MT/MTI
                   alternativnim nacinom #2, videti primer 3

    NUMBOX.CS - realizacija kontrole koja omogucava korisniku da unese broj
                sa formatiranim brojem decimala ili bez decimala, slicno
                EN kontroli u WinProxy ili GET polju sa numerickom varijablom
                u Clipper-u, kontrola je nezavisna od WinProxy-a i WP.CS-a

                                   
  Nacin komajliranja iz komandne linije:

    Primeri komajliranja mogu se videti u A.BAT uz svaki primer.
    Generalni oblik je:

      CSC [parametri1] <FILE1.CS> [<FILE2.CS>] [...] [parametri2]

    Neki od bitnijih parametara su:

      /t:exe     - pravi konzolni .EXE program, ovo je podrazumevano pa se
                   ne mora navoditi

      /t:winexe  - pravi graficki .EXE program (nema konzole)

      /t:library - pravi .DLL biblioteku

      /r:ime.DLL - omogucava programu koji se kompajlira da koristi sadrzaj
                   definisan u navedenoj .DLL biblioteci, navedeni .DLL mora
                   biti prisutan kad se rezultujuci .EXE pokrene

      /debug     - pravi .EXE/.DLL sa debug informacijama, bez ove opcije
                   nece biti poznata linija source koda u kojoj je nastupila
                   greska

      /optimize  - optimizuje kompajlirani .EXE/.DLL, upotrebljavati za
                   finalno kompajliranje pre isporuke programa

      /define:IME - definise IME pre kompajliranja, kao da je na pocetku
                    source koda napisano #define IME, sluzi za usmeravanje
                    uslovnog kompajliranja, ovim se opciono mogu ukljuciti
                    ili iskljuciti delovi source koda iz procesa kompajliranja

      /codepage:cp - ako tekst source koda sadrzi YU slova (tj. bilo koji znak
                     sa ASCII pozicijom vecom od 127) onda se ovom opcijom
                     mora naglasiti u kojoj kodnoj strani je taj tekst napisan,
                     ako se tekst kuca u nekom DOS editoru onda ce cp biti 852,
                     ako se kuca u nekom Windows editoru, onda ce cp biti 1250,

                     (za oba slucaja se podrazumeva da je u
                       Control Panel ->
                         Regional and Language Options ->
                           Advanced ->
                             Language for non-Unicode programs ->

                      podeseno:
                               
                        "Serbian (Latin)",
                        "Croatian"
                        "Slovenian"

                      ili neka druga vrsta YU kompatibilne latinice);

                     ako tekst source koda nema znakova sa ASCII kodom vecim
                     od 127 onda se ova opcija moze izostaviti

                     (videti primere 1, 2 ili 3 gde se koristio DOS editor i
                     ima YU slova) 

      @ime.RSP   - datoteke i opcije za kompajliranje su navedene u
                   tekst datoteci sa imenom ime.RSP


    Za kompajliranje source koda koji ce raditi sa WinProxy-jem generalno se 
    upotrebljavaju ova dva oblika:

      U toku razvoja:

        CSC file.cs wp.cs /debug /define:DEBUG

        (ovo kompajlira program koji se povezuje sa WPNetDbg.dll i ima dodatne
        informacije za debug konzolu (DbgView) i/ili debugger ako je prisutan)

        ova opcija odgovara "Debug" build konfiguraciji u Visual Studiju.

      Konacna verzija:

        CSC file.cs wp.cs /optimize

        (ovo kompajlira program koji se povezuje sa WPNet.dll i sa
        optimizovanijim kodom)

        ova opcija odgovara "Release" build konfiguraciji u Visual Studiju.


  U radu sa WinProxy-jem se upotrebljavaju sledeci osnovni tipovi:

    int - numericki tip bez decimala sa pribliznim opsegom +/- 2 000 000 000

    Double - numericki tip sa decimalama i priblizno 15 tacnih cifara

    String - za razliku od Clipper-ovog stringa, ovaj tip cuva znake u
             UNICODE formatu (dva bajta po znaku tj. 65536 kombinacija
             po znaku), literalni String se navodi sa " (ne moze sa ')

    Char - jedan znak String-a, kao string sa jednim UNICODE znakom,
           literalni Char se navodi sa ' (npr. 'A'), moze da se dobije iz
           String-a koriscenjem String-a kao niza npr. s[n] gde je s tipa
           String, a n moze biti broj od 0 za prvi znak, do s.Length - 1
           za zadnji znak u s

    DateTime - tip omogucuje vodjenje datuma i vremena, koristi se kao
               Clipper-ov ekvivalent za datumski tip s tim da se vreme u
               danu ignorise, ovaj tip nema neku vrstu null datuma (CTOD(""))
               pa se za te potrebe koristi minimalna vrednost ovog tipa
               koja se dobija sa DateTime.MinValue sto predstavlja
               datum 01.01.0001.

    bool - slicno Clipper-ovom logickom tipu, moze sadrzati true ili false
           

  Kratak opis klasa definisanih u WP.CS:


  WinProxy klasa:
  -----------------------------------------------------------------

    Na pocetku programa staviti using WP = WinProxy;
    tako se metode mogu koristiti samo sa prefiksom WP.

   Metode:

    MT <- WP.MTCreate( String dbdef );

    MT <- WP.MTLoad( String filename );

    MT <- WP.MTZipLoad( String zipfile, String filename );

    E1Pak <- WP.E1Prepare( MTI mti, 
                           String init_exp,
                           String while_exp,
                           String exec_exp,
                           String final_exp );

    E2Pak <- WP.E2Prepare( String prog, params MTBase[] mtbs );

    // primer upotrebe:  WP.E2Prepare( prog, mti1, mti2, mt1, mtbase );


    // genericke metode za alternativni nacin #2 pristupa poljima

    T <- WP.MTCreate<T>( String dbdef );

    T <- WP.MTLoad<T>( String filename );

    T <- WP.MTZipLoad<T>( String zipfile, String filename );



  MT klasa:
  -----------------------------------------------------------------

    Objekat MT klase predstavlja jednu memorijsku tabelu.

    Svaka MT poseduje sopstvenu tekucu poziciju, nezavisnu od 
    bilo koje druge MT ili MTI.

   Metode:

    (sve sto poseduje MTBase)

    MTI <- mt.Index( String key_expr );

    MTI[] <- mt.GetMTIs();      // vraca listu svih MTI nastalih iz ovog MT

    // genericka metoda za alternativni nacin #2 pristupa poljima

    T <- mt.Index<T>( String key_expr );


  MTI klasa:
  -----------------------------------------------------------------

    Objekat MTI klase predstavlja jedan indeks nad memorijskom tabelom.

    Svaka MTI poseduje sopstvenu tekucu poziciju, nezavisnu od 
    bilo koje druge MTI ili MT.

   Metode:

    (sve sto poseduje MTBase)

    bool <- mti.Filter( String filter_expr, int start, int end );
    bool <- mti.Filter( String filter_expr );
    bool <- mti.Filter( int start, int end );

    mti.RemoveFilter();

    int <- mti.IRowToRow( int irow );
    int <- mti.RowToIRow( int row );

    MT <- mti.GetMT();          // vraca MT iz kojeg je nastao ovaj MTI

    bool <- mti.Seek( String key, bool backward );
    bool <- mti.Seek( String key );

    bool <- mti.FilterKey( String key );  // uspostavlja filter koji zadrzava
                                          // samo slogove ciji indeksni kljuc
                                          // pocinje sa key
                                          // (brze radi nego mti.Filter)

    E1Pak <- mti.E1Prepare( String init_exp,
                            String while_exp,
                            String exec_exp,
                            String final_exp );


  MTBase klasa:
  -----------------------------------------------------------------

    Sadrzi presek sadrzaja MT i MTI, sve sto poseduje i MT i MTI se
    nalazi u ovoj klasi, omogucuje postojanje varijabli koje mogu da
    sadrze referencu i na MT i na MTI.

   Metode:

    int <- mtbase.RowPos(); 
    int <- mtbase.RowMax();

    mtbase.GoTo( int rowpos );

    bool <- mtbase.Skip( int delta ); // vraca true ako je pomeraj
                                      // u potpunosti uspeo
    mtbase.Append();
    mtbase.Modify();
    mtbase.Commit();

    mtbase.Delete();

    bool <- mtbase.Deleted();

    MTBookmark <- mtbase.SavePos();                   // snima RowPos poziciju
    bool <- mtbase.RestorePos( MTBookmark bookmark ); // vraca RowPos poziciju

    mtbase.Free();



   Metode izvedene iz MTRow klase:

    // citanje podataka sloga po rednom broju polja, prvo polje je 1

    String   <- mtbase.GetString   ( int field );  // za polja tipa C
    Double   <- mtbase.GetDouble   ( int field );  // za polja tipa N sa decimalama
    int      <- mtbase.GetInt      ( int field );  // za polja tipa N bez decimala
    DateTime <- mtbase.GetDateTime ( int field );  // za polja tipa D
    bool     <- mtbase.GetBool     ( int field );  // za polja tipa L

    Object   <- mtbase.GetObject( int field );  // vraca vrednost polja bez
                                                // obzira na tip polja

    // citanje podataka sloga po nazivu polja

    String   <- mtbase.GetString   ( String field_name ); // za polja tipa C
    Double   <- mtbase.GetDouble   ( String field_name ); // za polja tipa N sa decimalama
    int      <- mtbase.GetInt      ( String field_name ); // za polja tipa N bez decimala
    DateTime <- mtbase.GetDateTime ( String field_name ); // za polja tipa D
    bool     <- mtbase.GetBool     ( String field_name ); // za polja tipa L

    Object   <- mtbase.GetObject( String field_name );  // vraca vrednost polja
                                                        // bez obzira na tip

    // upis podataka sloga po rednom broju polja

    mtbase.Put( int field, String   value );   // za polja tipa C
    mtbase.Put( int field, Double   value );   // za polja tipa N
    mtbase.Put( int field, int      value );   // za polja tipa N
    mtbase.Put( int field, DateTime value );   // za polja tipa D
    mtbase.Put( int field, bool     value );   // za polja tipa L

    mtbase.Put( int field, Object value );  // upisuje vrednost polja
                                            // sadrzanog u Object tipu

    // upis podataka sloga po nazivu polja

    mtbase.Put( String field_name, String   value );   // za polja tipa C
    mtbase.Put( String field_name, Double   value );   // za polja tipa N
    mtbase.Put( String field_name, int      value );   // za polja tipa N
    mtbase.Put( String field_name, DateTime value );   // za polja tipa D
    mtbase.Put( String field_name, bool     value );   // za polja tipa L

    mtbase.Put( String field_name, Object value );  // upisuje vrednost polja
                                                    // sadrzanog u Object tipu
    int <- mtbase.FieldCount();

    String <- mtbase.FieldName( int field );

    Char <- mtbase.FieldType( int field );
    Char <- mtbase.FieldType( String field_name );

    int <- mtbase.FieldSize( int field );
    int <- mtbase.FieldSize( String field_name );

    int <- mtbase.FieldDecimals( int field );
    int <- mtbase.FieldDecimals( String field_name );

    String <- mtbase.FieldFormat( int field );           // vraca odgovarajuci
    String <- mtbase.FieldFormat( String field_name );   // format za N polja

    // npr. String.Format( mtbase.FieldFormat( "iznos" ),
    //                     mtbase.GetDouble( "iznos" ) );

    MTRow <- mtbase.GetRow();   // preuzima sadrzaj svih polja u novi MTRow objekat

    MTRow <- mtbase.GetBlankRow(); // kreira novi MTRow objekat sa default
                                   // vrednostima polja

    mtbase.PutRow( MTRow mtrow ); // upisuje sadrzaj mtrow u tekuci slog MT/MTI
                                  // mtbase, mtbase mora biti u MODIFY rezimu

    mtbase1.PutRow( MTBase mtbase2 ) // prepisuje sadrzaj svih polja tekuceg
                                     // sloga mtbase2 u tekuci slog mtbase1
                                     // mtbase1 mora biti u MODIFY rezimu

    bool <- mtbase.IsRowEqualTo( MTRow mtrow ); // vraca true ako su vrednosti
                                                // svih polja tekuceg sloga
                                                // jednake sa sadrzajem mtrow

    bool <- mtbase1.IsRowEqualTo( MTBase mtbase2 ); // vraca true ako su
                                                    // vrednosti svih polja
                                                    // tekuceg sloga mtbase1 i
                                                    // tekuceg sloga mtbase2
                                                    // jednake


  MTBookmark klasa:
  -----------------------------------------------------------------

    Omogucuje pamcenje tekuce pozicije za MT ili MTI. Bezbedno vraca
    poziciju nakon promene sadrzaja tabele ako je moguce ili se
    snalazi na najbolji moguci nacin ako nije moguce.

    Primer upotrebe:

      MTBookmark bm = mti.SavePos();  // kreira novi bookmark objekat
                                      // sa tekucom pozicijom mti
      // izmene u mti

      mti.RestorePos( bm );           // vraca poziciju mti na stanje
                                      // kada je kreiran bookmark


    Metode:

      mtbookmark.SavePos( MTBase mtbase );   // azurira postojeci bookmark
                                             // objekat sa tekucom pozicijom
                                             // MT/MTI



  E1Pak klasa:
  -----------------------------------------------------------------

   Metode:

     Object <- e1pak.Execute( String key, String param_exp, bool backward );
     Object <- e1pak.Execute( String key, String param_exp );
     Object <- e1pak.Execute( String key, bool backward );
     Object <- e1pak.Execute( String key );
     Object <- e1pak.Execute();

     e1pak.Free();


  E2Pak klasa:
  -----------------------------------------------------------------

   Metode:

     Object <- e2pak.Execute( String param, int istart, int iend );
     Object <- e2pak.Execute( String param );
     Object <- e2pak.Execute();

     bool <- e2pak.Filter( String param ); // vraca true ako filtriranje uspe
     bool <- e2pak.Filter();               // filtrira prvi alias dat u E2Prepare

     e2pak.Free();


  MTRow klasa:
  -----------------------------------------------------------------

    Postojanje MTRow klase omogucava da se sa svim poljima jednog
    sloga MT/MTI radi kao da se radi o jednom objektu. 
    
    Dobija se pozivom metode GetRow() na MT/MTI objektu sto izaziva
    kreiranje novog MTRow objekta i kopiranje vrednosti polja tekuceg
    sloga MT/MTI objekta u novokreirani MTRow objekat. Definicije polja
    se takodje kopiraju.
    
    Jednom izdvojeni podaci su nezavisni od MT/MTI objekta iz kojeg
    su izdvojeni (MT/MTI moze i da se zatvori, podaci u MTRow
    objektu ostaju).

    Podaci u MTRow objektu mogu da se citaju sa Get... funkcijama,
    ili da se u njega upisuju nove vrednosti sa Put... funkcijama,
    isto kao sa tekucim slogom MT/MTI objekta.

    Izmene koje se izvrse na MTRow objektu ne uticu na podatke u
    MT/MTI (ne azurira se izvorni objekat iz kojeg su podaci
    izdvojeni vec se izmene vrse samo na MTRow objektu).

    Metodom PutRow na MT/MTI objektu mogu se upisati vrednosti svih
    polja u tekuci slog MT/MTI. MT/MTI mora biti u MODIFY rezimu.

    Metodom IsRowEqualTo vrednosti svih polja se mogu uporediti izmedju
    dva MTRow objekta, MTRow objekta i tekuceg sloga MT/MTI, ili cak
    izmedju tekucih slogova dvaju MT/MTI.

   Metode:

    // citanje podataka sloga po rednom broju polja, prvo polje je 1

    String   <- mtrow.GetString   ( int field );  // za polja tipa C
    Double   <- mtrow.GetDouble   ( int field );  // za polja tipa N sa decimalama
    int      <- mtrow.GetInt      ( int field );  // za polja tipa N bez decimala
    DateTime <- mtrow.GetDateTime ( int field );  // za polja tipa D
    bool     <- mtrow.GetBool     ( int field );  // za polja tipa L

    Object   <- mtrow.GetObject( int field );  // vraca vrednost polja bez
                                                // obzira na tip polja

    // citanje podataka sloga po nazivu polja

    String   <- mtrow.GetString   ( String field_name ); // za polja tipa C
    Double   <- mtrow.GetDouble   ( String field_name ); // za polja tipa N sa decimalama
    int      <- mtrow.GetInt      ( String field_name ); // za polja tipa N bez decimala
    DateTime <- mtrow.GetDateTime ( String field_name ); // za polja tipa D
    bool     <- mtrow.GetBool     ( String field_name ); // za polja tipa L

    Object   <- mtrow.GetObject( String field_name );  // vraca vrednost polja
                                                        // bez obzira na tip

    // upis podataka sloga po rednom broju polja

    mtrow.Put( int field, String value   );   // za polja tipa C
    mtrow.Put( int field, Double value   );   // za polja tipa N
    mtrow.Put( int field, int value      );   // za polja tipa N
    mtrow.Put( int field, DateTime value );   // za polja tipa D
    mtrow.Put( int field, bool value     );   // za polja tipa L

    mtrow.Put( int field, Object value );  // upisuje vrednost polja
                                            // sadrzanog u Object tipu

    // upis podataka sloga po nazivu polja

    mtrow.Put( String field_name, String value   );   // za polja tipa C
    mtrow.Put( String field_name, Double value   );   // za polja tipa N
    mtrow.Put( String field_name, int value      );   // za polja tipa N
    mtrow.Put( String field_name, DateTime value );   // za polja tipa D
    mtrow.Put( String field_name, bool value     );   // za polja tipa L

    mtrow.Put( String field_name, Object value );  // upisuje vrednost polja
                                                    // sadrzanog u Object tipu

    int <- mtrow.FieldCount();

    String <- mtrow.FieldName( int field );

    Char <- mtrow.FieldType( int field );
    Char <- mtrow.FieldType( String field_name );

    int <- mtrow.FieldSize( int field );
    int <- mtrow.FieldSize( String field_name );

    int <- mtrow.FieldDecimals( int field );
    int <- mtrow.FieldDecimals( String field_name );

    String <- mtrow.FieldFormat( int field );           // vraca odgovarajuci
    String <- mtrow.FieldFormat( String field_name );   // format ispisa za N polja

    // npr. String.Format( mtrow.FieldFormat( "iznos" ),
    //                     mtrow.GetDouble( "iznos" ) );

    MTRow <- mtrow.GetRow();      // pravi novu nezavisnu kopiju MTRow objekta

    MTRow <- mtbase.GetBlankRow(); // kreira novi MTRow objekat sa default
                                   // vrednostima polja

    mtrow1.PutRow( MTRow mtrow2 );  // upisuje sadrzaj svih polja mtrow2
                                    // objekta u mtrow1 objekat

    mtrow.PutRow( MTBase mtbase ); // upisuje sadrzaj svih polja tekuceg sloga
                                   // MT/MTI u zadati MTRow objekat (cita
                                   // sadrzaj mtbase i azurira mtrow)

    bool <- mtrow1.IsRowEqualTo( MTRow mtrow2 ); // vraca true ako su vrednosti svih
                                                 // polja u dva MTRow objekta iste

    bool <- mtrow.IsRowEqualTo( MTBase mtbase ); // vraca true ako su vrednosti svih
                                                 // polja tekuceg sloga MT/MTI i
                                                 // zadatog mtrow objekta iste


  WinProxyUtilities klasa:
  -----------------------------------------------------------------

    Na pocetku programa staviti using WU = WinProxyUtilities;
    tako se metode mogu koristiti samo sa prefiksom WU.

   Metode:

    WU.Debug( Object o );
    WU.Debug( String format, params Object[] args );

      // Ispisuje sadrzaj na debug konzolu, primeri upotrebe
      //
      //      WU.Debug( mti.RowPos() );
      //      WU.Debug( "Ukupan iznos je {0}", mti.GetDouble("iznos") );
      //      WU.Debug( "{0:D}", DateTime.Today );

    DateTime <- WU.SToD( String s );
    String   <- WU.DToS( DateTime d );

    int <- WU.Tick  // sadrzi broj milisekundi od pokretanja Windows-a

    String <- WU.L2Bin( int n );  // konverzije su kompatibilne sa L2Bin i
    int <- WU.Bin2L( String s );  // Bin2L u E1/E2 programima

    String <- WU.Descend( String s );   // funkcija je kompatibilna sa descend
                                        // funkcijom u E1/E2 programima


  Kratak opis NumBox klase:


  NumBox klasa:
  -----------------------------------------------------------------

    Originalno .Net ima samo kontrolu za unos teksta TextBox, NumBox
    je realizacija kontrole za unos broja. NumBox kontrola sadrzi sve
    sto ima i TextBox plus sledecih nekoliko property-a:

      Double Value;   // sadrzi tekucu vrednost kontrole
                      // moze se i citati i menjati
                      // inicijalna vrednost je 0

      int Decimals;   // broj decimala
                      // moze se i citati i menjati
                      // moze biti od 0 do 15
                      // inicijalna vrednost je 0

      bool AllowNegative;  // da li se dozvoljava unos negativnih vrednosti
                           // moze se i citati i menjati
                           // inicijalna vrednost je true

      bool UseGroupSeparator;  // odredjuje da li ce celobrojni deo biti
                               // grupisan u po tri cifre, npr.

                               // true  ->  1,234,567.89
                               // false ->    1234567.89

                               // moze se i citati i menjati
                               // inicijalna vrednost je true

    NumBox kontrola je realizovana u NUMBOX.CS. Moze se kao source kod
    prikljuciti tekucem projektu ili komajlirati u poseban .DLL.
    Kao poseban .DLL se moze prikaciti na ToolBox paletu u Visual Studiju.

    Zahteva da Main funkcija programa sadrzi atribut [STAThread].
    Primer upotrebe NumBox klase je prikazan u primeru 5.

