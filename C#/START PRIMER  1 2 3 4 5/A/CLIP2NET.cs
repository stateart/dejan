ms-help://MS.W7SDK.1033/MS.W7SDKCOM.1033/xmlsdk/html/c8dd712b-48ff-4b97-a222-0c3bf63f1e99.htm



Ekvivalent funkcije obelezene sa * vazi samo ukoliko se upotrebljava
uz WinProxy.

Kao ekvivalent Clipper-ovog Array tipa podrazumevani su .NET tipovi
ArrayList ili List<T>. List<T> predstavlja listu objekata tipa T,
npr. List<String> predstavlja listu tipa String (moze da prima samo
elemente tipa String). ArrayList je vrlo priblizno List<Object>.
Posto varijabla tipa Object moze da prima vrednost bilo kojeg tipa
tako List<Object> (tj. ArrayList) moze da prima elemente bilo kojeg
tipa.

Ukoliko u napomeni nije drugacije naglaseno ekvivalent zahteva samo
System namespace. Sve funkcije koje koriste ArrayList zahtevaju i
System.Collections, dok funkcije koje koriste List<T> zahtevaju i
System.Collections.Generic namespace.

Koncept            Ekvivalent        Napomena
--------------------------------------------------------------------------------
Tipovi:

  n = 5            int n = 5;        ! numericki tip bez decimala
                                     ! opseg od -2147483648 do 2147483647
                                     ! zauzece memorije 4 bajta
                                     ! puno ime System.Int32

  n = 5.25         Double n = 5.25;  ! numericiki tip sa decimalama
                                     ! preciznost ~15 tacnih cifara
                                     ! zauzece memorije 8 bajta
                                     ! puno ime System.Double

  s = "abc"        String s = "abc"; ! string, UNICODE (svaki znak je dva bajta)
                                     ! zauzece memorije 2 bajta * duzina + ?
                                     ! puno ime System.String

  c = left(s,1)    Char c = 'a';     ! jedan znak UNICODE stringa
                                     ! zauzece memorije 2 bajta
                                     ! puno ime System.Char

  d = ctod("17.09.2011")    DateTime d = new DateTime( 2011, 9, 17 );

                                     ! datumski tip
                                     ! zauzece memorije ? (mozda 8 ili 12 bajta)
                                     ! puno ime System.DateTime

  b = (n > 7)      bool b = (n > 7); ! logicki tip, moze biti true ili false
                                     ! zauzece memorije 4 bajta
                                     ! puno ime System.Boolean

Logicke vrednosti bool tipa:

  .T.              true
  .F.              false

Kreiranje nove prazne liste:

  a = {}           ArrayList a = new ArrayList() ili List<T> a = new List<T>()

Sabiranje datuma:

  d2 = d1 + n      d2 = d1.AddDays( n )                        ! d1 mora biti DateTime

Oduzimanje datuma:

  n = d2 - d1      n = d2.Subtract( d1 ).Days;                 ! d1 i d2 moraju biti DateTime
--------------------------------------------------------------------------------


Funkcija           Ekvivalent            Napomena
--------------------------------------------------------------------------------
AADD(a,e)          a.Add(e)

ABS(n)             Math.Abs(n)

ACLONE(a)          new ArrayList(a) ili new List<T>(a)

ACOPY(a,at,s,n,t)  a.CopyTo( s, at, t, n )   ! za prvi element s ili t su 0

ADEL(a,n)          a.RemoveAt( n )       ! za prvi element n je 0

AEVAL(a,{|x| ...}) a.ForEach( delegate(Object x) { ... } ) za ArrayList
                   a.ForEach( delegate(T x) { ... } )      za List<T>

AINS(a,n)          a.Insert( n, e )      ! za prvi element n je 0, e je element koji se ubacuje

ALLTRIM(s)         s.Trim()

ASC(c)             (int) c               ! c mora biti Char

ASC(SUBSTR(s,n,1)) (int) s[n]            ! za prvi znak n je 0

ASCAN(a,e,s,n)     a.IndexOf(e,s,n)      ! za prvi element s je 0; vraca -1 ako nije nadjen;
                                         ! s i n se moze izostaviti
ASORT(a)           a.Sort()

AT(t,s)            s.IndexOf(t)          ! prvi znak je 0 ; vraca -1 ako nije nadjen

CDOW(d)*           d.ToString("ddd")     ! naziv dana je na srpskom, skracen (Pon, Uto, ...)

CDOW(d)*           d.ToString("dddd")    ! naziv dana je na srpskom

CHR(n)             new String( (Char) n, 1 )

CMONTH(d)*         d.ToString("MMM")     ! naziv meseca je na srpskom, skracen (Jan, Feb, ...)

CMONTH(d)*         d.ToString("MMMM")    ! naziv meseca je na srpskom

CTOD(s)*           DateTime.Parse(s)     ! izbacuje exception ukoliko nije ispravan format datuma

CURDIR()           Directory.CurrentDirectory()  ! vazi samo za CURDIR() bez parametara, zahteva System.IO

DATE()             DateTime.Today

DAY(d)             d.Day

DOW(d)             d.DayOfWeek           ! 0 - nedelja, 1 - ponedeljak, ... ,  6 - subota

DTOC(d)*           d.ToString("d")       ! "D" za cetvorocifrenu godinu

DTOC(d)            d.ToString("dd.MM.yy") ili d.ToString("dd.MM.yyyy")

FILE(s)            File.Exists(s)        ! zahteva System.IO

IF(b,t,f)          (b ? t : f)

INKEY(p)           Thread.Sleep(p*1000)  ! Thread.Sleep ceka odredjen broj milisekundi
                                         ! (ne reaguje na pritisak tastera, samo ceka)
                                         ! zahteva System.Threading

INT(n)             Math.Truncate(n)      ! n mora biti Double

LEFT(s,n)          s.Substring( 0, s.Length > n ? n : s.Length )

LEN(s)             s.Length              ! s mora biti String

LEN(a)             a.Count               ! a mora biti ArrayList ili List<T>

LOWER(s)*          s.ToLower()

LTRIM(s)           s.TrimStart()

MEMOREAD(f)        File.ReadAllText(f,Encoding.GetEncoding(852))    ! zahteva System.Text i System.IO
                                                                    ! tekst u datoteci se ocekuje
                                                                    ! u 852 kodnoj strani

MEMOWRIT(f,s)      File.WriteAllText(f,s,Encoding.GetEncoding(852)) ! zahteva System.Text i System.IO     
                                                                    ! tekst ce biti snimljen u
                                                                    ! 852 kodnoj strani

PADL(s,n,c)        s.PadLeft(n,c)        ! c mora biti Char, c moze da se izostavi

PADR(s,n,c)        s.PadRight(n,c)       ! c mora biti Char, c moze da se izostavi

REPLICATE(c,n)     new String( c, n )    ! c mora biti Char npr. new String( '*', 10 )

RIGHT(s,n)         s.Substring( s.Length > n ? s.Length - n : 0 )

ROUND(n,d)         Math.Round( n, d )    ! n mora biti Double, d moze da se izostavi

RTRIM(s)           s.TrimEnd()

SECONDS()          Environment.TickCount/1000   ! TickCount je broj milisekundi od kad je Windows pokrenut
                                                ! (ne od ponoci kao SECONDS() funkcija)

SPACE(n)           new String( ' ', n )

SQRT(n)            Math.Sqrt(n)          ! n mora biti Double

STR(n)*            n.ToString()          ! n mora biti int ili Double

STR(n,w)           String.Format( "{0,w}", n )     ! n mora biti int, w se upisuje u string

STR(n,w)           String.Format( "{0,w:F0}", n )  ! n mora biti Double, w se upisuje u string

STR(n,w,d)*        String.Format( "{0,w:Fd}", n )  ! n mora biti Double, w i d se upisuju u string

SUBSTR(s,p,n)      s.Substring( p, n )   ! za prvi znak p je 0, n moze da se izostavi

TIME()*            DateTime.Now.ToString("T")

TIME()             DateTime.Now.ToString("HH:mm:ss")

TRANSFORM(n,f)*    String.Format( f, n )  ! f nije kompatibilan sa Clipper-ovim formatom

   ! f je oblika "{p,w:Fd}" za format BEZ zareza izmedju hiljaditih ili
   !             "{p,w:Nd}" za format SA zarezima izmedju hiljaditih
   !
   !  w je sirina tj. ukupan broj znakova koji ce biti rezervisani za ispis
   !       broja, ako je w pozitivan, ispis se poravnava na desno
   !              ako je w negativan, ispis se poravnava na levo
   !
   !    w moze da se izostavi, tada se podrazumeva 0, sto znaci koliko god treba
   !    (ako nema w takodje ukloniti i zarez)
   !
   !  d je broj decimala
   !
   !    d moze da se izostavi, tada se korisiti podrazumevani broj decimala
   !
   !  p je pozicija parametra iza parametra f, za prvi parametar je 0,
   !  drugi 1, treci 2, ... ; vidi zadnji primer.

   ! Npr.

   !  n = 1234567890.777  f = "{0,20:F2}"   vraca: "       1234567890.78"

   !  n = 1234567890.777  f = "{0,20:N2}"   vraca: "    1,234,567,890.78"

   !  n = 1234567890.777  f = "{0,20:F0}"   vraca: "          1234567891"

   !  n = 1234567890.777  f = "{0,-20:N2}"  vraca: "1,234,567,890.78    "

   !  n = 1234567890.777  f = "{0,20:N4}"   vraca: "  1,234,567,890.7770"

   !  n = 1234567890.777  f = "{0,20:N0}"   vraca: "       1,234,567,891"

   !  n = 1234567890.777  f = "{0,5:F2}"    vraca: "1234567890.78"

   !  String.Format( "{2:F2}/{1:F2}/{0:F2}",
   !                 123.4567,
   !                 456.7890,
   !                 789.0123 )             vraca: "789.01/456.79/123.46"

UPPER(s)*          s.ToUpper()

YEAR(d)            d.Year
--------------------------------------------------------------------------------


