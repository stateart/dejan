using System;
using System.Collections;

public class test
{
  public static void Main()
  {
    ArrayList a;          // deklarise varijablu koja u sebi moze da sadrzi
                          // referencu na objekat klase ArrayList, inicijalno
                          // sadrzi vrednost null koja znaci da ne vrednost

    a = new ArrayList();  // kreira novi objekat klase ArrayList i
                          // njegovu adresu u memoriji dodeljuje varijabli a

    a.Add( "abc" );
    a.Add( 231 );
    a.Add( true );
    a.Add( new DateTime( 2011, 10, 15 ) );

    Console.WriteLine("a[0]={0}", a[0] );
    Console.WriteLine("a[1]={0}", a[1] );
    Console.WriteLine("a[2]={0}", a[2] );
    Console.WriteLine("a[3]={0:d}", a[3] );
    Console.WriteLine("a[3]={0:D}", a[3] );
  }
}
