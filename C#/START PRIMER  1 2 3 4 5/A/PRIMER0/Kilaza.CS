/**
 * Napisati klasu Covek koja sadrzi sledeca polja:
 * 		- ime
 * 		- pol (moze biti "muski" ili "zenski")
 * 		- visina (u cm)
 * 		- tezina (u kg)
 * 		- brojac u kom se prebrajaju kreirane instance klase
 * Klasi dodati i sledece metode:
 * 		- konstruktor koji prihvata ime, pol, visinu i tezinu coveka
 * 		- konstruktor bez parametara koji kreira coveka muskog pola sa imenom Petar,
 * 		  visine 175cm i tezine 65kg
 * 		- "optimalna" koji vraca optimalnu tezinu coveka. Optimalna tezina se racuna
 * 		  prema jednoj od sledece dve formule:
 * 			visina - 100 - ((visina - 150) / 4.0) za muskarce
 * 			visina - 100 - ((visina - 150) / 2.5) za zene
 * 		- "visak" koji vraca (true/false) da li covek ima visak kilograma
 * 
 * U glavnoj klasi kreirati nekoliko razlicitih ljudi, koristeci svaki od konstruktora
 * bar jednom. Zatim, izracunati i ispisati njihove optimalne tezine kao i informacije o 
 * tome da li imaju visak kilograma ili ne. Na kraju, ispisati ukupan broj kreiranih ljudi.
 */

class Covek
{
	String ime;
	String pol;
	int visina;
	float tezina;
	static int brojac;
	
	public Covek(String ime, String pol, int visina, int tezina)
	{
		this.ime = ime;
		this.pol = pol;
		this.visina = visina;
		this.tezina = tezina;
		++brojac;
	}
	
	public Covek()
	{
		this("Petar", "muski", 175, 65);
	}
	
	public float optimalna()
	{
		if (pol.equals("muski"))
			return visina - 100 - ((visina - 150) / 4.0f);
		return visina - 100 - ((visina - 150) / 2.5f);
	}
	
	public boolean visak()
	{
		return optimalna() < tezina;
	}
}

public class Kilaza
{
	private static void ispis(Covek c)
	{
		System.out.println(c.ime + ", " + c.pol + ", " + c.visina + "cm, " + c.tezina + "kg.");
		System.out.println("Optimalna tezina: " + c.optimalna() + "kg.");
		if (c.visak())
			System.out.println("Visak kilograma.");
		else
			System.out.println("Nema viska kilograma.");
		System.out.println();
	}
	

	public static void main(String[] args)
	{
		Covek covek1 = new Covek();
		Covek covek2 = new Covek("Dragana", "zenski", 160, 65);
		Covek covek3 = new Covek("Dusan", "muski", 195, 120);
		
		ispis(covek1);
		ispis(covek2);
		ispis(covek3);
		
		System.out.println("Ukupno je kreirano " + Covek.brojac + " ljudi.");
	}
}
