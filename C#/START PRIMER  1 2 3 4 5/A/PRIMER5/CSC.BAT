@echo off

rem ========================================================================
rem  Ova skalamerija sluzi da pronadje najnoviji instalirani .Net Framework
rem  koji sadrzi C# kompajler, kad ga pronadje, postavlja njegovu putanju u
rem  varijablu _cscpath.
rem ========================================================================

  set _cscpath=%systemroot%

  for /d %%d in (%systemroot%\Microsoft.Net\Framework\v*) do call :dir_check %%d

  if not exist %_cscpath%\csc.exe goto not_found

rem ========================================================================
rem  Kompajliranje sa kompajlerom iz _cscpath direktorijuma.
rem  Ovde podesiti parametre kompajliranja.
rem ========================================================================
  
  %_cscpath%\csc.exe %1 %2 %3 %4 %5 %6 %7 %8 %9

  if errorlevel 1 goto error

  set _cscpath=

rem ========================================================================
rem  Kompajliranje je uspelo.
rem  Ovde podesiti sta da se radi nakon uspesnog kompajliranja.
rem ========================================================================




  goto :eof


:error

  set _cscpath=

rem ========================================================================
rem  Greska pri kompajliranju.
rem ========================================================================



  goto :eof

:not_found

  echo.
  echo Nije pronadjen C# kompajler.

  goto :eof

rem ========================================================================
rem  Ova batch funkcija proverava pojedinacni framework direktorijum.
rem  %1 je kandidat direktorijum. Ako ispunjava uslove postaje novi _cscpath
rem ========================================================================

:dir_check

  if not exist %1\csc.exe goto :eof

  if /i "%_cscpath%" geq "%1" goto :eof

  set _cscpath=%1

  goto :eof


